#!/usr/bin/python

from __future__ import print_function
import os
import sys
import unittest
from subprocess import check_call

def to_components(path):
    """
    Split a path into its components
    """
    abs_path = os.path.abspath(path)
    return abs_path.split(os.sep)

def starts_with(a_list, prefix_list):
    """
    Returns true if a_list starts with prefix_list
    """
    return a_list[:len(prefix_list)] == prefix_list

def readable(components):
    """
    Make path components readable in error
    """
    return '<' + '/'.join(components) + '>'

def allowed_sync(source, destination):
    """
    Ensure that destination is in the same data project as source,
    and under Data-Bris-Copy/deposits

    """
    source_components = to_components(source)
    destination_components = to_components(destination)

    export_index = source_components.index('export')

    if source_components[export_index + 1] != 'Data-Bris':
        raise Exception("Couldn't find Data-Bris directory in {}".format(readable(source_components)))

    expected_parent = source_components[:export_index] + ['Data-Bris-Copy','deposits']

    if not starts_with(destination_components, expected_parent):
        raise Exception("Destination {} does not have expected parent {}".format( 
            readable(destination_components), readable(expected_parent)))
    elif len(expected_parent) != len(destination_components) - 1:
        raise Exception("Destination {} is not at top level of Data-Bris-Copy/deposits".format(
            readable(destination_components)))
    else:
        return True

if __name__ == '__main__':

    if len(sys.argv) != 3:
        print("Use: restricted_sync source destination", file=sys.stderr)
        sys.exit(1)

    source = sys.argv[1]
    destination = sys.argv[2]

    try:
        # Guard
        if not allowed_sync(source, destination):
            print("Sync prohibited", file=sys.stderr)
            sys.exit(2)
        
        check_call(["rsync", "-a", "--delete-excluded", "--exclude", ".*", source, destination])
        check_call(["chown", "-R", "tomcat:tomcat", destination])

    except Exception as e:
        print("Error syncing: {}".format(e), file=sys.stderr)
        sys.exit(3)

### Simple sanity tests ###

class Tests(unittest.TestCase):
    def test_no_export(self):
        self.assertRaises(Exception, allowed_sync, 'a/b/c', 'd')

    def test_no_export_data_bris(self):
        self.assertRaises(Exception, allowed_sync, 'a/b/export/d', 'd')

    def test_no_common_project(self):
        self.assertRaises(Exception, allowed_sync, 'a/b/export/Data-Bris/foo', 'd')

    def test_no_common_project(self):
        self.assertRaises(Exception, allowed_sync, 'a/b/export/Data-Bris/foo', 'd')

    def test_too_deep(self):
        self.assertRaises(Exception, allowed_sync, 'a/b/export/Data-Bris/foo', 'a/b/Data-Bris-Copy/deposits/a/b')

    def test_should_work(self):
        self.assertTrue(allowed_sync('a/b/export/Data-Bris/foo', 'a/b/Data-Bris-Copy/deposits/xxx'))

    def test_dots_ignored(self):
        self.assertTrue(allowed_sync('a/b/export/../export/Data-Bris/foo', 'a/b/Data-Bris-Copy/deposits/xxx'))
