from fabric.operations import run, put, sudo
from fabric.api import *
from fabric.context_managers import lcd
from fabric.contrib.files import upload_template
from os.path import dirname
import os

env.hosts = ['data-bris.acrc.bris.ac.uk']

def deploy():
	stop_tomcat()
	remove_old()
	deploy_war()
	stop_tomcat()

def stop_tomcat():
	sudo('/etc/init.d/tomcat6 stop')

def start_tomcat():
	sudo('/etc/init.d/tomcat6 start')

def remove_old():
	sudo('rm -r /var/lib/tomcat6/webapps/dundry')

def deploy_war():
	 # stash old one, just in case
	sudo('cp /var/lib/tomcat6/webapps/dundry.war /home/cmdms/dundry-old.war')
	put('target/dundry-0.1-SNAPSHOT.war', '/var/lib/tomcat6/webapps/dundry.war', use_sudo=True)
