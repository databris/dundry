<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:exslt-strings="http://exslt.org/strings"
  version="1.0">
  <xsl:output method="text"/>

  <xsl:template match="node()[foaf:sha1]">
    <xsl:value-of select="foaf:sha1"/>
    <xsl:text>  </xsl:text>
    <xsl:value-of select="exslt-strings:decode-uri(substring(@rdf:about, 6))"/>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>
  
  <xsl:template match="text()|@*"/>

</xsl:stylesheet>