#!/bin/sh

#if [[ -z "$DATACITE_PASSWORD" ]]
#then
#	echo "DATACITE_PASSWORD not set"
#	exit 1
#fi

ID=$1

if [[ -z "$ID" ]]
then
	echo I need a deposit ID argument to work
	exit 1
fi 

DATACITE_ENDPOINT=https://mds.datacite.org/doi

UPDATE="doi=10.5523/bris.$ID
url=http://data.bris.ac.uk/data/dataset/$ID
"

echo Update:
echo -e $UPDATE

curl --trace output -u BL.BRISTOL $DATACITE_ENDPOINT --data-binary "$UPDATE" -H "Content-type: text/plain"
