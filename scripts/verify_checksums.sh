#!/bin/sh

#set -x

set -e

HERE=$(cd $(dirname "$0") && pwd)
THERE=$(basename "$1")

# Extract checksums from rdf file
XSL="$HERE/get_checksums.xsl"

# Go to parent of $1 - metadata includes ID
cd $1/..

# Make checksum file, and verify with shasum
xsltproc "$XSL" "$THERE/.info.rdf" | sha1sum --quiet -c -

find -H "$THERE" -type f -not -name '.*' | wc -l
xsltproc "$XSL" "$THERE/.info.rdf" | wc -l
