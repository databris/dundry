package uk.ac.bristol.dundry;

import com.google.common.collect.ImmutableSet;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.syntax.ElementData;
import org.apache.jena.sparql.syntax.ElementGroup;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Set;
import javax.xml.bind.DatatypeConverter;
import org.apache.jena.iri.IRI;
import org.apache.jena.iri.IRIFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class Util {

    final static Logger log = LoggerFactory.getLogger(Util.class);

    final static IRIFactory IRIFACTORY = IRIFactory.iriImplementation();

    /**
     * Put a path under a resource.
     *
     * For example http://example.com/foo and a/b would return a resource
     * ...foo/a/b
     *
     * @param a Labelled resource to append against
     * @param b Path to append
     * @return
     */
    public static Resource append(Resource a, Path b) {
        // if a is blank, then complain!
        if (a.isAnon()) {
            throw new IllegalArgumentException("Cannot append against a blank node");
        }

        // A shortcut, which also avoids a/b/1234 becoming a/b/1234/
        if (b.toString().isEmpty()) {
            return a;
        }
        
        String encPath = escapePath(b);
        
        // We want to put these paths under a, so ensure it has a '/' on the end
        String iri = a.getURI().endsWith("/") ?
                a.getURI() :
                a.getURI() + "/";

        IRI resolved = IRIFACTORY.construct(iri).resolve(encPath);
        return a.getModel().createResource(resolved.toString());
    }
    
    /**
     * Take a path and make a resource
     * 
     * For absolute paths this will be a file URI, otherwise a relative URI.
     * (Is the former even useful?)
     * 
     * @param m Model resource is associated with
     * @param p Path to convert
     * @return 
     */
    public static Resource toResource(Model m, Path p) {
        if (p.isAbsolute()) {
            return m.createResource(p.toUri().toASCIIString());
        } else {
            return m.createResource(escapePath(p));
        }
    }
    
    /**
     * URI escape a Path as URI path component
     * 
     * @param p
     * @return 
     */
    private static String escapePath(Path p) {
        try {
            URI x = new URI(null, null, p.toString(), null, null);
            return x.toASCIIString();
        } catch (URISyntaxException ex) {
            throw new RuntimeException("Issue encoding path " + p, ex);
        }
    }
    
    public static String hexDigest(Path path, String algorithm) throws IOException, NoSuchAlgorithmException {
        byte[] digest;
        
        try (InputStream in = Files.newInputStream(path, StandardOpenOption.READ)) {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            DigestInputStream digester = new DigestInputStream(in, md);
            byte[] buffer = new byte[1024 << 6];
            while (digester.read(buffer) != -1) {
            }
            digest = digester.getMessageDigest().digest();
        }
        
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }
    
    public static String getHashNIO(Path path, String hashAlgorithm) throws IOException, NoSuchAlgorithmException {
    
        MessageDigest md = MessageDigest.getInstance(hashAlgorithm);
        try (FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
            ByteBuffer bbf = ByteBuffer.allocateDirect(1024 << 6);
            
            int b = fc.read(bbf);
            
            while ((b != -1) && (b != 0)) {
                bbf.flip();
                md.update(bbf);
                bbf.clear();
                b = fc.read(bbf);
            }
        }
        
        byte[] digest = md.digest();
        
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

    /**
     * Recursively copy a directory into another directory.
     * Copying a to b will result in b/a...
     * @param from Source
     * @param to Destination
     * @return Path to copied directory
     * @throws IOException
     */
    public static Path copyDirectory(final Path from, final Path to) throws IOException {
        log.debug("Copy {} to {}", from, to);
        final Path parent = from.getParent();
        try {
            Files.walkFileTree(from, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Path rel = parent.relativize(file);
                    log.trace("Visit {}", file);
                    Files.copy(file, to.resolve(rel), StandardCopyOption.COPY_ATTRIBUTES);
                    return FileVisitResult.CONTINUE;
                }
                
                // Create directory first, so target directory is available
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    Path rel = parent.relativize(dir);
                    log.trace("Visit dir {}", dir);
                    log.trace("Create dir {}", to.resolve(rel));
                    Files.createDirectory(to.resolve(rel));
                    return FileVisitResult.CONTINUE;
                }
            });
            
            return to.resolve(from.getFileName());
        } catch (IOException ex) {
            throw ex;
        }
    }
    
    /**
     * Symlink target from directory base
     * 
     * @param base Directory to put link in
     * @param target Target of link
     * @throws IOException 
     */
    public static void symlink(Path base, Path target) throws IOException {
        // Link has same name as root, but under link base
        Path link = base.resolve(target.getFileName());
        // We have some sym links shennanigans, so get the real path to avoid
        // multiple indirections
        Path absTarget = target.toRealPath();
        
        log.info("Link target <{}> as <{}>", absTarget, link);

        // Check whether we already have this
        if (Files.exists(link) 
                && Files.isSymbolicLink(link) 
                && Files.readSymbolicLink(link).equals(absTarget)) {
            log.info("Link {} already in exists", link);
            return;
        }
        
        try {
            Files.createSymbolicLink(link, absTarget);
        } catch (IOException ex) {
            log.error("Issue linking " + absTarget + " to " + link, ex);
            throw ex;
        }
    }
    
    /**
     * Query a model
     * 
     * @param model Model to query
     * @param query Query to execute
     * @return Copy of result (no need to free resources)
     */
    public static ResultSet query(Model model, Query query) {
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet res = ResultSetFactory.copyResults(qe.execSelect());
        qe.close();
        return res;
    }
    
    /**
     * Load a query from resources
     * 
     * @param queryLocation
     * @return 
     */
    public static Query loadQuery(String queryLocation) {
        return QueryFactory.read(queryLocation);
    }
    
    // Ignore these file names
    final static Set<String> IGNORE = 
            ImmutableSet.of(".info.rdf", ".info.json", ".header.html", ".footer.html",".zipinfo");
    
    /**
     * Detect whether file is not part of deposit
     * 
     * (i.e. artifact of publication system)
     * 
     * @param file
     * @param id A unique id
     * @return true is file is considered ignorable (e.g. .info.rdf, or contains id)
     */
    public static boolean isIgnorableFile(Path file, String id) {
        String filename = file.getFileName().toString();
        
        return IGNORE.contains(filename) || filename.contains(id);
    }
    
    /**
     * Take a query and some value bindings, and return a query where
     * the values are provided in a VALUES block
     * 
     * Roughly: { pattern } => { VALUES ... pattern }
     * 
     * @param unboundQuery Original query
     * @param preBind Values to bind
     * @return Equivalent query with provided values bound
     */
    public static Query prebind(Query unboundQuery,  Binding preBind){
        // Our new pattern
        ElementGroup queryPattern = new ElementGroup();
        
        // Provide the values in a data block
        ElementData valuesBlock = new ElementData();
        // Ensure all vars are present
        Iterator<Var> varIt = preBind.vars();
        while (varIt.hasNext()) {
            valuesBlock.add(varIt.next());
        } 
        valuesBlock.add(preBind);
        
        // Add values and original pattern as siblings
        queryPattern.addElement(valuesBlock);
        queryPattern.addElement(unboundQuery.getQueryPattern());
        
        // Clone the original query, then replace the pattern
        Query boundQuery = QueryFactory.create(unboundQuery);
        boundQuery.setQueryPattern(queryPattern);
        
        return boundQuery;
    }
}
