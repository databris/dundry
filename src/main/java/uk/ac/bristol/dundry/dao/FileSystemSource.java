package uk.ac.bristol.dundry.dao;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.model.FileTree;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class FileSystemSource {
    
    final static Logger log = LoggerFactory.getLogger(FileSystemSource.class);
    
    public final static FileTree NONE = new FileTree();
    
    private final Path root;
    
    public FileSystemSource(String base) {
        root = Paths.get(base);
    }
    
    /**
     * Resolve path relative to the root of this source
     * It considers '/' to be the root
     * @param relative
     * @return 
     */
    public Path getPath(String relative) {
        // Remove leading '/'
        String relativised = relative.replaceFirst("^/+", "");
        
        Path relPath = root.resolve(relativised).normalize();
        
        // Is the path now below root?
        if (!relPath.startsWith(root)) {
            throw new RuntimeException("Path " + relative + " not under root");
        }
        return relPath;
    }
    
    /**
     * Get a file tree rooted at root/base
     * @param base
     * @return
     * @throws IOException 
     */
    public FileTree getTreeAt(String base, int maxDepth, int maxBaseItems, boolean calcSize) 
            throws IOException {
        Path path = getPath(base);
        log.debug("Get path contents for <{}> ({})", base, path);
        return getTreeAt(path, maxDepth, maxBaseItems, calcSize);
    }
    
    /**
     * Get a file tree rooted at root/base
     * @param start
     * @param maxDepth Maximum depth of folders to traverse
     * @param maxEntries Maximum number of entries per folder to collect
     * @param calcSize Calculate file and folder sizes accurately
     * @return
     * @throws IOException 
     */
    public FileTree getTreeAt(Path start, int maxDepth, int maxBaseItems, boolean calcSize) 
            throws IOException {
        // This will happen if you try to get a non-existent root
        // (unless the directory listing goes stale?)
        if (!Files.exists(start)) return NONE;
                
        ListingVisitor lv = new ListingVisitor(root, maxDepth, maxBaseItems, calcSize);
        
        // If we don't want to calculate size we can tell walkFileTree to skip deep bits
        int maxD = calcSize ? Integer.MAX_VALUE : maxDepth ;
        
        Files.walkFileTree(start, Collections.EMPTY_SET, maxD, lv);
        
        return lv.getTree();
    }
    
    static class ListingVisitor implements FileVisitor<Path> {
        
        final FileTree treeRoot; // Our special root node
        FileTree parent; // Current parent as we traverse tree
        final Deque<FileTree> stack; // Stack going up tree, to remember parents
        final int depthLimit; // Beyond this depth we only collect size
        final int itemLimit; // Beyond this number of siblings we collect only size
        int depth; // current depth
        private boolean inLeafDir; // at bottom in a dir
        private final Path root; // Relativise paths to this root
        private final boolean calcSize; // Include subtrees beyond depth limit 
        
        ListingVisitor(Path root, int depthLimit, int itemLimit, boolean calcSize) {
            this.root = root;
            this.depthLimit = depthLimit;
            this.itemLimit = itemLimit;
            this.calcSize = calcSize;
            this.inLeafDir = false;
            
            this.stack = new ArrayDeque<>();
            this.depth = 1;
            
            this.treeRoot = new FileTree("ROOT", "ROOT", true, false, 0);
            
            stack.push(treeRoot);
            
            parent = treeRoot;
        }
        
        FileTree getTree() {
            return treeRoot.getFirstChild();
        }
        
        private FileTree tryAddEntry(Path path, boolean isDirectory, long size) {
            
            Path relPath = root.relativize(path);
            
            if (depth > depthLimit || (parent.isPartial && !isDirectory)) { // beyonds limits: ignore
                return null;
            } else if (!isDirectory && parent.getFileCount() == itemLimit) {
                // at depth limit, enforce item limit
                parent.isPartial = true; // we have limited this listing
                return null;
            } else { // within depth
                FileTree newItem = new FileTree(
                        relPath.toString(), relPath.getFileName().toString(), 
                        isDirectory, Files.isReadable(path), size);
                
                parent.add(newItem);
                
                return newItem;
            }
        } 
        
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            
            FileTree newDir = 
                    tryAddEntry(dir, true, 0);
            
            if (!calcSize && inLeafDir) {
                return FileVisitResult.SKIP_SUBTREE;
            }
            
            if (depth < depthLimit) { // && newDir != null) {
                stack.push(parent);                
                parent = newDir;
            } else if (newDir != null) {
                // We are entering a leaf dir
                // (tryAddEntry not null, depth == depthLimit)
                inLeafDir = true;
                stack.push(parent);                
                parent = newDir;
            }
                        
            depth++;
            
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            
            // This seems to happen if we SKIP_SUBTREEs
            // subtrees are vistFile'd rather than pre- and postVisitDir..
            if (attrs.isDirectory()) {
                // this does nothing, since we skip at the limit
                // but safer to add it in
                tryAddEntry(file, true, 0);
                return FileVisitResult.CONTINUE;
            }
            
            parent.size += attrs.size();
            
            tryAddEntry(file, false, attrs.size());
                        
            return FileVisitResult.CONTINUE;
        }
        
        // Happens with unreadable Dirs
        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            log.info("Can't visit path: <{}>", file);
            
            tryAddEntry(file, true /* assume is directory */, 0);
            
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            
            depth--;
            
            if (depth < depthLimit) { // && inLeafDir) {
                long subSize = parent.size;
                parent = stack.pop();
                parent.size += subSize;
            } else if (inLeafDir && depth == depthLimit) {
                // We are leaving a leaf directory
                long subSize = parent.size;
                parent = stack.pop();
                parent.size += subSize;
                inLeafDir = false;
            }
                        
            return FileVisitResult.CONTINUE;
        }
    }
}
