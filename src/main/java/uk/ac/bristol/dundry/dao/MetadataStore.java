package uk.ac.bristol.dundry.dao;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public interface MetadataStore {

    void create(String graphId, Model initialContent);

    Model getData(String graphId);

    ResultSet query(Query query);
    
    boolean ask(Query query);
    
    void replaceData(String graphId, Model content);

    void create(String toInternalId);
    
    void update(String update);
    
    Dataset dump();
}
