package uk.ac.bristol.dundry.dao;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.update.GraphStore;
import org.apache.jena.update.GraphStoreFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class MetadataStoreDS implements MetadataStore {
    private final Dataset store;
    
    public MetadataStoreDS(Dataset store) {
        this.store = store;
    }
    
    @Override
    public void create(String graphId) {
        // nothing to do here. Created on demand.
    }
    
    @Override
    public void create(String graphId, Model initialContent) {
        store.getNamedModel(graphId).add(initialContent);
    }

    @Override
    public Model getData(String graphId) {
        return ModelFactory.createDefaultModel().add(store.getNamedModel(graphId));
    }

    @Override
    public ResultSet query(Query query) {
        QueryExecution qe = QueryExecutionFactory.create(query, store);
        try {
            store.begin(ReadWrite.READ);
            ResultSet r = qe.execSelect();
            return ResultSetFactory.copyResults(r);
        } finally {
            qe.close();
            store.end();
        }
    }
    
    @Override
    public boolean ask(Query query) {
        QueryExecution qe = QueryExecutionFactory.create(query, store);
        try {
            store.begin(ReadWrite.READ);
            return qe.execAsk();
        } finally {
            qe.close();
            store.end();
        }
    }
    
    @Override
    public void update(String updateString) {
        UpdateRequest update = UpdateFactory.create(updateString);
        UpdateProcessor up = UpdateExecutionFactory.create(update, store);
        try {
            store.begin(ReadWrite.WRITE);
            up.execute();
            store.commit();
        } finally {
            store.end();
        }
    }

    @Override
    public void replaceData(String graphId, Model content) {
        try {
            store.begin(ReadWrite.WRITE);
            store.getNamedModel(graphId).removeAll().add(content);
            store.commit();
        } finally {
            store.end();
        }
    }
    
    @Override
    public Dataset dump() {
        return store;
    }
}
