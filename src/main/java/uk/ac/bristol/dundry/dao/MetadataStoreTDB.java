package uk.ac.bristol.dundry.dao;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class MetadataStoreTDB implements MetadataStore {
    
    protected final Dataset store;
    
    public MetadataStoreTDB(String location) {
        store = TDBFactory.createDataset(location);
    }
    
    @Override
    public void create(String graphId) {
        // nothing to do here. Created on demand.
    }
    
    @Override
    public void create(String graphId, Model initialContent) {
        try {
            store.begin(ReadWrite.WRITE);
            store.getNamedModel(graphId).add(initialContent);
            store.commit();
        } finally {
            store.end();
        }
    }
    
    @Override
    public void replaceData(String graphId, Model model) {
        try {
            store.begin(ReadWrite.WRITE);
            store.getNamedModel(graphId).removeAll().add(model);
            store.commit();
        } finally {
            store.end();
        }
        
    }
    
    @Override
    public Model getData(String graphId) {
        // Return a copy of the data, to ensure modifications occur via
        // replace data
        try {
            store.begin(ReadWrite.READ);
            return ModelFactory.createDefaultModel().add(store.getNamedModel(graphId));
        } finally {
            store.end();
        }
    }

    @Override
    public ResultSet query(Query query) {
        try (QueryExecution qe = QueryExecutionFactory.create(query, store)) {
            store.begin(ReadWrite.READ);
            ResultSet r = qe.execSelect();
            return ResultSetFactory.copyResults(r);
        } finally {
            store.end();
        }
    }
    
    @Override
    public boolean ask(Query query) {
        try (QueryExecution qe = QueryExecutionFactory.create(query, store)) {
            store.begin(ReadWrite.READ);
            return qe.execAsk();
        } finally {
            store.end();
        }
    }
    
    @Override
    public void update(String updateString) {
        UpdateRequest update = UpdateFactory.create(updateString);
        UpdateProcessor up = UpdateExecutionFactory.create(update, store);
        try {
            store.begin(ReadWrite.WRITE);
            up.execute();
            store.commit();
        } finally {
            store.end();
        }
    }
    
    @Override
    public Dataset dump() {
        // I'd like a protected copy, but may be large
        return store;
    }
}
