package uk.ac.bristol.dundry.dao;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.DCTypes;
import org.apache.jena.vocabulary.RDFS;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import org.quartz.Job;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.model.ResourceCollection;
import uk.ac.bristol.dundry.tasks.JobBase;
import uk.ac.bristol.dundry.tasks.MoveTask;
import uk.ac.bristol.dundry.tasks.StateChanger;
import uk.ac.bristol.dundry.vocabs.Bibo;
import uk.ac.bristol.dundry.vocabs.RepositoryVocab;
import uk.ac.bristol.dundry.webresources.providers.RDFSerialiser;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class Repository {
    
    static final Logger log = LoggerFactory.getLogger(Repository.class);
    
    public static enum State {
        Created, Depositing, Deposited, PendingPublish, Publishing, Published, Deleted       
    }
    
    // play it safe. radix of 36 is ideal 
    static final int RADIX = Math.min(Character.MAX_RADIX, 36);
    
    @Autowired
    protected TaskManager taskManager;
    
    // Don't want this here -- task should be returned not run here
    @Autowired
    protected FileSystemSource fileSystemSource;
    
    @Autowired
    public RDFSerialiser rdfSerialiser;
    
    private final FileRepository fileRepo;
    private final MetadataStore mdStore;
    private final List<Class<? extends Job>> postDepositJobs;
    private final List<Class<? extends Job>> prePublishJobs;
    private final List<Class<? extends Job>> preRestrictedPublishJobs;
    private final Properties jobProperties;
    
    /**
     * The heart of the repository
     *
     * @param fileRepo The
     * @param mdStore The metadata store
     * @param postDepositJobClasses A list of classes (implementing Job) which
     * will be run when content is added
     * @param prePublishJobClasses A list of classes which will be run
     * pre-publication
     * @param jobProperties Parameters that will be passed to the jobs
     * @param sensitiveProperties Parameters that will be added to the jobProperties
     */
    public Repository(
            FileRepository fileRepo, MetadataStore mdStore,
            List<String> postDepositJobClasses,
            List<String> prePublishJobClasses,
            List<String> preRestrictedPublishJobClasses,
            Properties jobProperties,
            Properties sensitiveProperties) {
        this.fileRepo = fileRepo;
        this.mdStore = mdStore;
        this.postDepositJobs = getJobsFromClassNames(postDepositJobClasses);
        this.prePublishJobs = getJobsFromClassNames(prePublishJobClasses);
        this.preRestrictedPublishJobs = getJobsFromClassNames(preRestrictedPublishJobClasses);
        this.jobProperties = jobProperties;
        for (Entry<Object, Object> secret: sensitiveProperties.entrySet()) {
            jobProperties.setProperty((String) secret.getKey(), (String) secret.getValue());
        }
    }
    
    final static Query GET_DEPOSITS = Util.loadQuery("queries/get_deposits.rq");
    
    public ResourceCollection getIds() {
        Model resultModel = ModelFactory.createDefaultModel();
        ResultSet r = mdStore.query(GET_DEPOSITS);
        List<Resource> ids = new LinkedList<>();
        
        // Keep track of subject, to handle multiple values
        Resource currentItem = null;
        while (r.hasNext()) {
            QuerySolution nxt = r.next();
            // get item and copy to resultModel
            Resource item = nxt.getResource("g").inModel(resultModel);
            
            // New item found
            if (!item.equals(currentItem)) {
                currentItem = item;
                ids.add(item);
            }
            
            item.addProperty(RDFS.label, nxt.get("title"));
            item.addProperty(RepositoryVocab.state, nxt.get("state"));
            if (nxt.contains("source")) {
                item.addProperty(DCTerms.hasPart, nxt.get("part"));
                item.getModel().add(nxt.get("part").asResource(), DCTerms.source, nxt.get("source"));
            }
            if (nxt.contains("description")) {
                item.addProperty(DCTerms.description, nxt.get("description"));
            }
            if (nxt.contains("project")) {
                item.addProperty(RepositoryVocab.project, nxt.get("project"));
            }
            if (nxt.contains("available")) {
                item.addProperty(DCTerms.available, nxt.get("available"));
            }
            if (nxt.contains("type")) {
                item.addProperty(DCTerms.type, nxt.get("type"));
            }
        }
        log.debug("Ids is: {}", ids);
        return new ResourceCollection(ids);
    }

    public boolean hasId(String item) {
        // There will always be a prov graph if ID is registered
        Query existQuery = QueryFactory.create(
            String.format("ask { graph <%s/prov> {} }",
                toInternalId(item)
            )
        );
        
        return mdStore.ask(existQuery);
    }
    
    public List<RDFNode> find(Query query) {
        ResultSet results = mdStore.query(query);
        
        // We only expect one variable
        String firstVar = results.getResultVars().get(0);
        
        // First var -> list
        List<RDFNode> toReturn = Lists.newArrayList();
        while (results.hasNext()) {
            toReturn.add(results.next().get(firstVar));
        }
        
        return toReturn;
    }
    
    /**
     * Create a new deposit
     *
     * @param creator User id who made this deposit
     * @param subject Metadata to include about the created deposit. It will be
     * renamed once and id has been allocated.
     * @return
     * @throws IOException
     */
    public String create(String creator, Resource subject) throws IOException, SchedulerException {
        // Create a random id!
        UUID randId = UUID.randomUUID();
        String id =
                Long.toUnsignedString(randId.getMostSignificantBits(), RADIX)
                + Long.toUnsignedString(randId.getLeastSignificantBits(), RADIX);

        // Now we have an id rename the subject
        Resource renamed = ResourceUtils.renameResource(subject, toInternalId(id));
        
        // Get base dir. It is allowed to be missing if using absolute repo paths
        String base = (renamed.hasProperty(RepositoryVocab.base_directory)) ?
                renamed.getProperty(RepositoryVocab.base_directory).getString() :
                null;
        
        Path repoDir = fileRepo.create(id, base);

        Resource prov = ModelFactory.createDefaultModel().createResource(toInternalId(id));
        prov.addLiteral(DCTerms.dateSubmitted, Calendar.getInstance());
        prov.addProperty(RepositoryVocab.depositor, creator);
        if (base != null) prov.addProperty(RepositoryVocab.base_directory, base);
        prov.addProperty(RepositoryVocab.state, State.Created.name());

        // Create mutable and immutable graphs
        mdStore.create(toInternalId(id), renamed.getModel()); // often a noop
        mdStore.create(toInternalId(id) + "/prov", prov.getModel());

        return id;
    }
    
    public List<Path> getDepositContents(String id) throws IOException {
        // If published we need to use the publish location
        
        Path root;
        
        switch (getState(id)) {
            case Published: root = getPublishPathForId(id); break;
            case Deposited: root = getDepositPathForId(id); break;
            default: root = null; // Not safe to answer
        }
        
        if (root == null) return Collections.EMPTY_LIST;
        
        // Only list visible items
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(root, notInvisible)) {
            return ImmutableList.copyOf(ds);
        }
    }
    
    // No files with names starting with '.'
    public final static DirectoryStream.Filter notInvisible =
            new DirectoryStream.Filter<Path>() {

        @Override
        public boolean accept(Path t) throws IOException {
            return !t.getFileName().toString().startsWith(".");
        }
    };
    
    /**
     * Put content into the repository.
     * 
     * This just sets the source, but kicks off post-deposit jobs which
     * should sync content
     *
     * @param id The repository id
     * @param source Source for the source (will be recorded with
     * deposit)
     */
    public void setSource(String id, String source) throws SchedulerException { 
        // We permit source change even in deposited state
        ensureState(id, EnumSet.of(State.Created, State.Deposited));
        
        Resource prov = getProvenanceMetadata(id);
        
        prov.removeAll(DCTerms.source);
        
        prov.addProperty(DCTerms.source, source);
        
        startProcess(id, prov,
                State.Depositing, State.Deposited,
                Collections.EMPTY_LIST, postDepositJobs, jobProperties
                );
    }
    
    public void publish(String id) throws SchedulerException {
        ensureState(id, EnumSet.of(State.Deposited, State.PendingPublish));
        
        // If available date is ok, publish immediately
        if (shouldPublishNow(id)) {
            log.info("Publishing {} immediately", id);
            runPublish(id);
        } else {
            log.info("Defer publishing {} until requested date", id);
            setState(id, State.PendingPublish); // Come back later
        }
    }
    
    /**
     * Emergency function to remove a dataset from public view
     * @param id 
     */
    public void hide(String id) throws IOException {
        ensureState(id, EnumSet.of(State.Published));
        
        String publishBase = jobProperties.getProperty("publish.base");
        Path publicLink = Paths.get(publishBase, id);
        log.warn("Attempting to restrict access to {}", publicLink);
        Set<PosixFilePermission> currentPermissions = 
                Files.getPosixFilePermissions(publicLink);
        currentPermissions.remove(PosixFilePermission.OTHERS_READ);
        currentPermissions.remove(PosixFilePermission.OTHERS_EXECUTE);
        Files.setPosixFilePermissions(publicLink, currentPermissions);
    }
    
    public void runPublish(String id) throws SchedulerException {
        ensureState(id, EnumSet.of(State.Deposited, State.PendingPublish));
        
        Resource prov = getProvenanceMetadata(id);
        Resource item = getMetadata(id);
        
        // Remember when this was published
        prov.addLiteral(DCTerms.date, Calendar.getInstance());    
        updateProvenanceMetadata(id, prov);
        
        Resource accessType = item.getPropertyResourceValue(DCTerms.type);
        
        // Move deposit to restricted path (if required)
        String target = this.getFinalDataPathForId(id, accessType).toString();
        
        // Add the move task. This is the primary task.
        List<Class<? extends Job>> jobs = new ArrayList<>();
        jobs.add(MoveTask.class);
        
        // If 
        if (accessType == null || DCTypes.Dataset.equals(accessType)) {
            jobs.addAll(prePublishJobs);
        } else {
            jobs.addAll(preRestrictedPublishJobs);
        }
        
        // Include move task properties at the end
        startProcess(id, prov,
                State.Publishing, State.Published, Collections.EMPTY_LIST,
                jobs, jobProperties, 
                MoveTask.FROM, getDepositPathForId(id).toAbsolutePath().toString(),
                MoveTask.TO, target);
    }
    
    protected boolean shouldPublishNow(String id) {
        // Find out whether this is for immediate publication
        Resource item = getMetadata(id);
                
        Statement available = item.getProperty(DCTerms.available);
        
        if (available == null) {
            log.warn(
                "Item {} has no dc:available release date, defaulting ok to publish.",
                 item);
            return true;
        }
                
        // Available is set by user, and so typically un-typed
        LocalDate publishDate = ISODateTimeFormat.date().
                parseLocalDate(available.getLiteral().getLexicalForm());
                
        LocalDate now = new LocalDate();
                
        // Has publish date passed yet?
        return !now.isBefore(publishDate);
    }
    
    /**
     * Soft-delete a record.
     * @param id The repository id
     */
    public void delete(String id) {
        ensureState(id, EnumSet.of(State.Created, State.Deposited));
        
        Resource prov = getProvenanceMetadata(id);
        prov.removeAll(RepositoryVocab.state);
        prov.addLiteral(RepositoryVocab.state, State.Deleted.name());
        updateProvenanceMetadata(id, prov);
    }
    
    /**
     * Start a process to move between states.
     * Less abstractly, kick off tasks that will result in deposit or publication
     * @param id ID of the deposit
     * @param prov Provenance metadata to store (a bit hacky?)
     * @param startState State during this process (e.g. Depositing)
     * @param endState State at end of this process (e.g. Deposited)
     * @param jobsToRun External jobs to run (will run first)
     * @param jobs Internal jobs
     * @param jobParams Externally provided parameters
     * @param otherParams Any other parameters (in pairs)
     */
    private void startProcess(String id, Resource prov,
            State startState, State endState,
            List<JobDetail> jobsToRun,
            List<Class<? extends Job>> jobs, Properties jobParams,
            Object... otherParams) throws SchedulerException {
        
        JobDataMap jobData = new JobDataMap();
        
        jobData.put(JobBase.ID, id);
        jobData.put(JobBase.REPOSITORY, this); // Not keen on this (not storable)
        jobData.put(JobBase.FILESOURCE, this.fileSystemSource); // ditto
        
        jobData.put(StateChanger.TO_STATE, endState.name());
        
        // Copy in job params
        for (Entry<Object, Object> e : jobParams.entrySet()) {
            jobData.put((String) e.getKey(), (String) e.getValue());
        }
        // And add other params
        for (int i = 0; i < otherParams.length - 1; i += 2) {
            jobData.put((String) otherParams[i], otherParams[i + 1]);
        }

        List<JobDetail> jobDetails = new ArrayList<>();
        jobDetails.addAll(jobsToRun);
        
        for (Class<? extends Job> c: jobs) {
            JobDetail jd = newJob(c)
                    .withIdentity(c.getCanonicalName(), id)
                    .usingJobData(jobData)
                    .build();
            jobDetails.add(jd);
        }
        
        // Add the state changer at the end
        jobDetails.add(newJob(StateChanger.class)
                .withIdentity("state changer", id)
                .usingJobData(jobData).build());
        
        // Go to start state, store provenance
        // Not keen on this going over a method boundary, but simplifies things
        prov.removeAll(RepositoryVocab.state);
        prov.addLiteral(RepositoryVocab.state, startState.name());
        updateProvenanceMetadata(id, prov);
        
        // Make it so
        taskManager.executeJobsInOrder(id, jobDetails);
    }
    
    /**
     * Gets all know information about id
     * 
     * @param id
     * @return 
     */
    public Resource getFullMetadata(String id) {
        String internalId = toInternalId(id);

        Model m = ModelFactory.createDefaultModel();
        m.add(mdStore.getData(internalId));
        m.add(mdStore.getData(internalId + "/prov"));
        m.setNsPrefixes(rdfSerialiser.getDefaultPrefixes());
        return m.createResource(internalId);
    }
    
    /**
     * Gets only the externally editable data concerning id
     * 
     * @param id
     * @return 
     */
    public Resource getMetadata(String id) {
        String internalId = toInternalId(id);

        Model m = ModelFactory.createDefaultModel();
        m.add(mdStore.getData(internalId));
        m.setNsPrefixes(rdfSerialiser.getDefaultPrefixes());
        
        Resource res = m.createResource(internalId);
        
        // Query for important prov info we always need
        Query getStateQuery = QueryFactory.create(
                String.format(
                        "select ?state ?doi ?source where "
                                + "{ graph <%1$s/prov> { <%1$s> <%2$s> ?state "
                                + "optional { <%1$s> <%3$s> ?doi } "
                                + "optional { <%1$s> <%4$s> ?source } } }", 
                        internalId,
                        RepositoryVocab.state,
                        Bibo.doi,
                        DCTerms.source
                )
        ); // TODO replace with jena selectbuilder when in maven
        
        ResultSet r = mdStore.query(getStateQuery);
        
        // If found, add those specific bits in
        if (r.hasNext()) {
            QuerySolution results = r.next();
            res.addProperty(RepositoryVocab.state, results.get("state"));
            if (results.contains("doi")) {
                res.addProperty(Bibo.doi, results.get("doi"));
            }
            if (results.contains("source")) {
                res.addProperty(DCTerms.source, results.get("source"));
            }
        }
        
        return res;
    }
    
    /**
     * Replace editable information about id with r
     * 
     * @param id
     * @param r 
     */
    public void updateMetadata(String id, Resource r) {
        String internalId = toInternalId(id);

        // Replace metadata with new information that's not in prov
        Model m = ModelFactory.createDefaultModel();
        m.add(r.getModel());
        
        // Ensure these non-edittable fields aren't present
        m.removeAll(null, Bibo.doi, null);
        m.removeAll(null, RepositoryVocab.state, null);
        m.removeAll(null, DCTerms.source, null);
        
        // Remove provenance info
        m.remove(mdStore.getData(internalId + "/prov"));
        mdStore.replaceData(internalId, m);
        
        // If already published update indexes and datacite
        if (State.Published == getState(id)) {
            log.info("Item {} published, metadata updated. Republishing metadata.", id);
            
            Resource item = getMetadata(id);
            
            Resource accessType = item.getPropertyResourceValue(DCTerms.type);
            
            try {
                
                List<String> tasks;
                
                if (this.getAccessLevel(id, accessType) == 0) {
                    tasks = Arrays.asList(
                            "uk.ac.bristol.dundry.tasks.GenerateApacheIndexes",
                            "uk.ac.bristol.dundry.tasks.ZipTask",
                            "uk.ac.bristol.dundry.tasks.DataCiteSubmit");
                } else { // remember to republish contact page
                    tasks = Arrays.asList(
                            "uk.ac.bristol.dundry.tasks.PublishContactPage",
                            "uk.ac.bristol.dundry.tasks.GenerateApacheIndexes",
                            "uk.ac.bristol.dundry.tasks.DataCiteSubmit");
                }
                
                this.executeTasks(id, tasks);
                
            } catch (Exception e) {
                log.error("Issue republishing metadata for " + id, e);
            }
        }
    }

    public Resource getProvenanceMetadata(String id) {
        String internalId = toInternalId(id);

        return mdStore.getData(internalId + "/prov").createResource(internalId);
    }

    public void updateProvenanceMetadata(String id, Resource r) {
        String internalId = toInternalId(id);

        mdStore.replaceData(internalId + "/prov", r.getModel());
    }

    public Path getDepositPathForId(String id) {
        return fileRepo.depositPathForId(id, getBase(id));
    }

    public Path getPublishPathForId(String id) {
        return fileRepo.publishPathForId(id, getBase(id));
    }
    
    final static List<Resource> ACCESS_TYPES = Arrays.asList(
        DCTypes.Dataset, 
        RepositoryVocab.RestrictedDataset,
        RepositoryVocab.ControlledDataset,
        RepositoryVocab.ClosedDataset
    );
    
    // This returns the ultimate destination of a deposit given an access type
    private Path getFinalDataPathForId(String id, Resource accessType) {
        
        int accessLevel = getAccessLevel(id, accessType);
        
        // Lowest level is same as publish path
        return accessLevel == 0 ? 
                getPublishPathForId(id) :
                fileRepo.restrictedPublishPathForId(id, getBase(id), accessLevel);
    }
    
    private int getAccessLevel(String id, Resource accessType) {
        
        if (accessType == null) {
            log.warn("<{}> has no access type. Assuming lowest.", id);
            return 0;
        } else {
            // Not very efficient, but we don't call it very often
            return ACCESS_TYPES.indexOf(accessType);
        }
    }
    
    /**
     * This is the path where the data currently resides. This varies through the process,
     * and is a little complicated for restricted publications.
     * 
     * @param id
     * @return The path where the data can be found
     */
    public Path getDataPathForId(String id) {
        Resource item = getFullMetadata(id);
        
        State state = getState(item);
        
        // Get type
        Resource accessType = item.getPropertyResourceValue(DCTerms.type);
                        
        if (state == State.Published || state == State.Publishing) {
            return getFinalDataPathForId(id, accessType) ;
        } else {
            // before publishing life is much simpler
            return getDepositPathForId(id);
        }
    }
    
    /**
     * Returns the status of a deposit
     * @param id
     * @return 
     */
    public State getState(String id) {
        // Query directly - this is much faster 
        Query getStateQuery = QueryFactory.create(
                String.format(
                        "select ?state where { graph <%1$s/prov> { <%1$s> <%2$s> ?state } }", 
                        toInternalId(id), 
                        RepositoryVocab.state.getURI()
                )
        );
        
        ResultSet r = mdStore.query(getStateQuery);
        
        // This shouldn't happen - we check
        if (!r.hasNext()) {
            throw new RuntimeException("No state found for " + id);
        }
        
        String state = r.next().getLiteral("state").getString();
        
        return State.valueOf(state);
    }
    
    private State getState(Resource r) {
        return State.valueOf(r.getRequiredProperty(RepositoryVocab.state).getString());
    }
    
    /**
     * Sets the status of a deposit
     * @param id 
     * @param state The new state
     */
    public void setState(String id, State state) {
        Resource provenance = getProvenanceMetadata(id);
        // Remove existing state
        provenance.removeAll(RepositoryVocab.state);
        // Set new state
        provenance.addProperty(RepositoryVocab.state, state.name());
        updateProvenanceMetadata(id, provenance);
    }
    
    public String getBase(String id) {
        Resource r = getProvenanceMetadata(id);
        Statement base = r.getProperty(RepositoryVocab.base_directory);
        return (base == null) ? null : base.getString();
    }
    
    /**
     * Takes an id and makes it suitable for external use by stripping off
     * leading base if present
     *
     * @param uri
     * @return An un-repo'd string
     */
    public String toExternalId(String uri) {
        if (uri.startsWith(this.rdfSerialiser.getBase())) {
            return uri.substring(this.rdfSerialiser.getBase().length());
        } else {
            return uri;
        }
    }
    
    // Detect whether URI has a scheme (and hence is not relative)
    final static Pattern IS_REL = 
            Pattern.compile("^[a-zA-z][a-zA-Z0-9+\\-.]*:");
    
    /**
     * Make an internal uri from id
     *
     * @param id
     * @return
     */
    public String toInternalId(String id) {
        if (IS_REL.matcher(id).find()) {
            return id;
        } else {
            return this.rdfSerialiser.getBase() + id;
        }
    }

    // For initialising the jobs
    private List<Class<? extends Job>> getJobsFromClassNames(List<String> classNames) {
        // Load up job classes
        List<Class<? extends Job>> jobs = new ArrayList<>();
        for (String jobClassName : classNames) {
            // Try to load the class. Check it is a Job.
            try {
                Class<?> job = Repository.class.getClassLoader().loadClass(jobClassName);
                if (Job.class.isAssignableFrom(job)) {
                    jobs.add((Class<? extends Job>) job);
                } else {
                    log.error("Class <{}> is not a Job. Ignoring.", jobClassName);
                }
            } catch (ClassNotFoundException ex) {
                log.error("Job class <{}> not found. Ignoring.", jobClassName);
            }
        }

        return jobs;
    }
    
    /**
     * Check whether the state of id is one of allowed
     * @param id
     * @param allowed 
     */
    private void ensureState(String id, EnumSet<State> allowed) {
        State state = this.getState(id);
        if (!allowed.contains(state)) 
            throw new IllegalArgumentException(String.format(
                    "Not permitted in current state <%s> (allowed %s)",
                    state,
                    allowed
                    ));
    }
    
    /**
     * A special 'out of band, be careful' way to run tasks
     * For admins only! Task may not work properly, depending on state of the
     * deposit (e.g. files may be not be in expected positions)
     * @param taskClassnames 
     */
    public void executeTasks(String id, List<String> taskClassnames) throws Exception {
        List<Class<? extends Job>> jobs = getJobsFromClassNames(taskClassnames);
        
        JobDataMap jobData = new JobDataMap();
        jobData.put(JobBase.ID, id);
        jobData.put(JobBase.REPOSITORY, this);
        jobData.put(JobBase.FILESOURCE, this.fileSystemSource);
        
        // Copy in job params
        for (Entry<Object, Object> e : jobProperties.entrySet()) {
            jobData.put((String) e.getKey(), (String) e.getValue());
        }
        
        for (Class<? extends Job> job: jobs) {
            JobBase inst = (JobBase) job.newInstance();
            inst.execute(jobData);
        }
    }
    
    // Cron tasks. Not convinced I want them at all.
    
    public static class ScheduledTask<T extends Job> {
        public final String cron;
        public final Class<T> job;
        
        public ScheduledTask(String cron, String jobClass) throws ClassNotFoundException {
            this.cron = cron;
            this.job = (Class<T>) ScheduledTask.class.getClassLoader().loadClass(jobClass);
        }
    } 
    
    public void setScheduledTasks(List<ScheduledTask> tasks) throws SchedulerException {
        for (ScheduledTask schedTask: tasks) {
            
            JobDataMap jobData = new JobDataMap();
            jobData.put(JobBase.REPOSITORY, this);
            jobData.put(JobBase.FILESOURCE, this.fileSystemSource);
            
            JobDetail job = newJob(schedTask.job)
                    .usingJobData(jobData)
                    .build();
            
            Trigger trigger = newTrigger()
                    .withSchedule(cronSchedule(schedTask.cron))
                    .build();
            
            taskManager.scheduleTriggeredJob(trigger, job);
        }
    }
}
