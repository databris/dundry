package uk.ac.bristol.dundry.dao;

import com.google.common.collect.Lists;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.annotation.PreDestroy;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import org.quartz.Job;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.EverythingMatcher;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.listeners.JobChainingJobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.model.ResourceCollection;
import uk.ac.bristol.dundry.tasks.ChainTerminator;
import uk.ac.bristol.dundry.tasks.JobBase;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@Component
public class TaskManager {
    
    static final Logger log = LoggerFactory.getLogger(TaskManager.class);
    
    private final Scheduler scheduler;
    
    public TaskManager() throws SchedulerException {
        this.scheduler = StdSchedulerFactory.getDefaultScheduler();
        this.scheduler.start();
    }
    
    @PreDestroy
    public void destroy() throws SchedulerException {
        log.info("Shutting down task manager");
        scheduler.shutdown();
    }
    
    public List<Object> listAllTasks() throws SchedulerException {
        List<Object> stuff = new ArrayList<>();
        stuff.add("-- Executing --");
        stuff.addAll(scheduler.getCurrentlyExecutingJobs());
        stuff.add("------");
        stuff.addAll(scheduler.getJobKeys(GroupMatcher.jobGroupContains("")));
        stuff.add("---- Listeners -----");
        stuff.addAll(scheduler.getListenerManager().getJobListeners());
        return stuff;
    }
    
    public ResourceCollection listTasks(String id) throws SchedulerException {
        final Model m = ModelFactory.createDefaultModel();
        Set<JobKey> keys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(id));
        
        Collection<Resource> tasks = Lists.newArrayListWithCapacity(keys.size());
        
        for (JobKey key: keys) {
            JobDetail detail = scheduler.getJobDetail(key);
            
            if (detail != null) { // This has happened
                Resource job = m.createResource();
                if (detail.getDescription() != null)
                    job.addProperty(DCTerms.description, detail.getDescription());
                job.addProperty(DCTerms.title, key.getName());
                tasks.add(job);
            } else {
                log.warn("Job with key {} has no job detail", key);
            }
        }
        
        return new ResourceCollection(tasks);
    }
    
    /**
     * Execute a series of jobs
     * 
     * @param id
     * @param jobs
     * @throws SchedulerException 
     */
    public void executeJobsInOrder(String id, List<JobDetail> jobs) throws SchedulerException {
        if (jobs.isEmpty()) {
            return;
        }
        
        // Add all defaultJobs to scheduler
        for (JobDetail jd: jobs) scheduler.addJob(jd, true);
        
        // Make a chain of these defaultJobs 
        String chainId = "chain-" + id;
        JobChainingJobListener jcl = new JobChainingJobListener(chainId);
        for (int i = 0; i < jobs.size() - 1; i++) {
            jcl.addJobChainLink(jobs.get(i).getKey(), jobs.get(i + 1).getKey());
        }
        
        // Once complete the chain listener just hangs around. This last job will
        // remove it
        JobDetail terminator = newJob(ChainTerminator.class)
                .usingJobData(ChainTerminator.LISTENERID, chainId)
                .build();
        
        // Schedule for termination
        scheduler.addJob(terminator, true);
        jcl.addJobChainLink(jobs.get(jobs.size() - 1).getKey(), terminator.getKey());
        
        // Add chain listener to scheduler
        scheduler.getListenerManager().addJobListener(jcl, EverythingMatcher.allJobs());
        
        // Start the first job!
        scheduler.triggerJob(jobs.get(0).getKey());
    }
    
    public void scheduleTriggeredJob(Trigger trigger, JobDetail job) throws SchedulerException {
        scheduler.scheduleJob(job, trigger);
    }
}