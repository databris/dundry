package uk.ac.bristol.dundry.exceptions;

/**
 *
 * @author cmdms
 */
public class NotFoundException extends RuntimeException {
    
    public NotFoundException(String message, Object missingThing) {
        super(String.format("%s: missing <%s>", message, missingThing));
    }
    
}
