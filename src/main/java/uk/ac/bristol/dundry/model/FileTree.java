package uk.ac.bristol.dundry.model;

import com.google.common.collect.Iterables;
import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@XmlRootElement
public class FileTree implements Comparable<FileTree> {
    
    @XmlElement
    public final String path;
    @XmlElement
    public final String name;
    // Don't serialise this -- not useful in end product
    public final boolean isDirectory;
    @XmlElement
    public final boolean isReadable;
    @XmlElement
    protected final Collection<FileTree> children;
    @XmlElement
    protected final Collection<FileTree> contents;
    @XmlElement
    public long size;
    @XmlElement
    public boolean isPartial;
    
    private int fileCount;
    
    public FileTree() {
        this(null, null, false, false, -1);
    }
    
    public FileTree(String path, String name, boolean isDirectory, 
            boolean isReadable, long size) {
        this.path = path;
        this.name = name;
        this.isDirectory = isDirectory;
        this.isReadable = isReadable;
        this.children = isDirectory ? new TreeSet<>() : Collections.EMPTY_SET;
        this.contents = isDirectory ? new TreeSet<>() : Collections.EMPTY_SET;
        this.size = size;
        this.isPartial = false;
        this.fileCount = 0;
    }
    
    public FileTree getFirstChild() {
        return Iterables.getFirst(children, null);
    }
    
    public void add(FileTree child) {        
        if (child.isDirectory) {
            children.add(child);
        } else {
            this.fileCount++;
            contents.add(child);
        }
    }
    
    public int getFileCount() {
        return fileCount;
    }
    
    @Override
    public String toString() {
        return buildString("", new StringBuilder()).toString();
    }
    
    private StringBuilder buildString(String indent, StringBuilder in) {
        in.append(indent).append("[").append(name).append(' ');
        in.append(isDirectory ? 'D' : ' ');
        in.append(isReadable ? ' ' : '!');
        in.append(isPartial ? 'P' : ' ');
        in.append(' ').append(size);
        for (FileTree f: contents) {
            in.append(f.name);
            in.append(isReadable ? ' ' : '!');
            in.append(',');
        }
        if (isPartial) {
            in.append(indent).append(" ...\n");
        }
        in.append("]\n");
        for (FileTree c: children) c.buildString(indent + " ", in);
        return in;
    }

    @Override
    public int compareTo(FileTree o) {
        if (o.isDirectory == this.isDirectory) {
            return this.name.compareTo(o.name);
        } else if (o.isDirectory) {
            return 1;
        } else {
            return -1;
        }
    }
}
