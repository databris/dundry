package uk.ac.bristol.dundry.model;

import org.apache.jena.rdf.model.Resource;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

/**
 * A wrapper for resource collections to reify the generic collection and thus
 * help the Provider.
 *  
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public final class ResourceCollection extends AbstractCollection<Resource> {
    private final Collection<Resource> coll;
    private Model model;
    
    public ResourceCollection(Collection<Resource> coll) {
        this.coll = coll;
        
        if (coll.isEmpty()) {
            model = ModelFactory.createDefaultModel();
        } else {
            // Use first model
            model = coll.stream().findFirst().get().getModel();
                        
            // Check all resources share the same model
            if (!coll.stream().allMatch(r -> r.getModel() == model)) {
                throw new IllegalArgumentException("Resources must share same model");
            }
        }
    }
    
    @Override
    public final Iterator<Resource> iterator() {
        return coll.iterator();
    }

    @Override
    public final int size() {
        return coll.size();
    }
    
    public Model getModel() {
        return model;
    }
}
