package uk.ac.bristol.dundry.tasks;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;

/**
 * Utility class to run command line processes in a reasonably sane way
 * 
 * @author pldms
 */
public class Command {
    
    final static org.slf4j.Logger log = LoggerFactory.getLogger(Command.class);
    
    /**
     * Run a command line process, logging stdout and stderr
     * 
     * @param workingDir Directory to run in
     * @param command
     * @throws JobExecutionException On process error (e.g. returning non-zero)
     */
    public static void run(Path workingDir, String... command) throws JobExecutionException {
        
        log.info("Running command: '{}'", Joiner.on("' '").join(command));
        
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.directory(workingDir.toFile());
        
        Process process;
        try {
            process = pb.start();
        } catch (IOException ex) {
            throw new JobExecutionException("Failed to start process", ex);
        }
        
        try (
            BufferedReader stdout = readerFromInStream(process.getInputStream(), Charsets.UTF_8);
            BufferedReader stderr = readerFromInStream(process.getErrorStream(), Charsets.UTF_8);
                ) {
            
            // Pause a little bit, then log output, until process is finished
            while (alive(process)) {
                Thread.sleep(500);
                
                // Log output
                logStream(stdout, false);
                logStream(stderr, true);
            }
            
            if (process.exitValue() > 0) {
                String message = String.format("Command: %s failed, exit code: %s",
                        pb.command(), process.exitValue());
                throw new JobExecutionException(message);
            }
        } catch (InterruptedException intEx) {
            throw new JobExecutionException("Process wait interrupted", intEx);
        } catch (IOException ioEx) {
            throw new JobExecutionException("IO issue in process run", ioEx);
        }
        
    }
    
    /**
     * Make a buffered reader from an input stream
     * 
     * @param in
     * @param charset
     * @return 
     */
    private static BufferedReader readerFromInStream(InputStream in, Charset charset) {
        return new BufferedReader(new InputStreamReader(in, charset));
    }
    
    /**
     * Clear out the buffer to the log
     * 
     * @param reader
     * @param isWarn if true, log level is 'warn', otherwise 'debug'
     * @throws IOException 
     */
    private static void logStream(BufferedReader reader, boolean isWarn) throws IOException {
        while (reader.ready()) {
            if (isWarn) { log.warn(reader.readLine()); }
            else { log.debug(reader.readLine()); }
        }
    }
    
    /**
     * Check whether a process is still running
     * 
     * @param process
     * @return true if still running
     */
    private static boolean alive(Process process) {
        // Pre-java 8 this doesn't exist, so we have to do this instead.
        // but it transpires this is identical to the java 8 implementation :-)
        try {
            process.exitValue();
            return false;
        } catch (IllegalThreadStateException ex) {
            return true;
        }
    }
    
    public static void main(String[] args) {
        try {
            Command.run(Paths.get("."), "ls", "/tmp");
        } catch (Exception e) {
            System.out.printf("Error running: '%s'\n", e);
        }
    }
}
