package uk.ac.bristol.dundry.tasks;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDFS;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Properties;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.vocabs.Bibo;
import uk.ac.bristol.dundry.vocabs.RepositoryVocab;
import uk.ac.bristol.dundry.vocabs.Schema;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class DataCiteSubmit extends JobBase {

    final static Logger log = LoggerFactory.getLogger(DataCiteSubmit.class);
    final static XMLOutputFactory xof = XMLOutputFactory.newInstance();
    final static Query getFormats = QueryFactory.create(
            "select distinct ?format { ?s <http://purl.org/dc/terms/format> ?format }");

    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        String username = jobData.getString("datacite.username");
        String password = jobData.getString("datacite.password");
        String endpoint = jobData.getString("datacite.endpoint");
        String doiprefix = jobData.getString("datacite.doiprefix");
        boolean testing = jobData.getBoolean("datacite.testing");
        String landingprefix = jobData.getString("datacite.landingprefix");

        submit(username, password, endpoint, doiprefix, testing,
                id, landingprefix, repo.getFullMetadata(id), prov);
    }

    // Broken out for testing purposes
    public void submit(String username, String password, String endpoint,
            String doiprefix, boolean testing, String id, String landingprefix,
            Resource fullItem, Resource prov) throws JobExecutionException {

        // Include testmode param if we are testing
        String testMode = testing ? "?testMode=true" : "";

        String mdEndpoint = endpoint + "metadata" + testMode;
        String doiEndpoint = endpoint + "doi" + testMode;

        String doi = doiprefix + id;
        String url = landingprefix + id;

        // see https://test.datacite.org/mds/static/apidoc
        CredentialsProvider cp = new BasicCredentialsProvider();
        cp.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));

        try (CloseableHttpClient httpClient = HttpClients.custom()
                // Ugh, datacite (test?) is redirecting on POST
                .setRedirectStrategy(new LaxRedirectStrategy())
                .setDefaultCredentialsProvider(cp)
                .build()) {

            handleResponse(submitMetadata(httpClient, mdEndpoint, doi, fullItem));

            if (!fullItem.hasProperty(Bibo.doi)) {
                handleResponse(submitDOI(httpClient, doiEndpoint, doi, url));
                // Record this permanently
                prov.addProperty(Bibo.doi, doi);
            }
        } catch (MalformedURLException ex) {
            throw new JobExecutionException("URL is malformed", ex);
        } catch (IOException ex) {
            throw new JobExecutionException("Issue communicating with datacite", ex);
        } catch (XMLStreamException ex) {
            throw new JobExecutionException("Problem writing metadata record xml for datacite", ex);
        }
    }

    // Handle the result of an http request
    private void handleResponse(HttpResponse response) throws JobExecutionException, IOException {
        try {
            int sc = response.getStatusLine().getStatusCode();
            if ((sc / 100) != 2) {
                log.error("Request failed: {} ({})\n{}", new Object[]{
                    response.getStatusLine().getReasonPhrase(), sc,
                    EntityUtils.toString(response.getEntity())});
                throw new JobExecutionException("Error with datacite: " + response.getStatusLine().getReasonPhrase());
            } else if (log.isDebugEnabled()) { // log the response if debugging
                log.debug("Request succeeded ({}):\n{}\n", sc, EntityUtils.toString(response.getEntity()));
            }
        } finally {
            // Ensure connection is finished with
            EntityUtils.consume(response.getEntity());
        }
    }

    // Submit datacite metadata
    public HttpResponse submitMetadata(CloseableHttpClient client, String endpoint,
            String doi, Resource item) throws XMLStreamException, IOException {

        StringWriter out = new StringWriter();
        XMLStreamWriter writer = xof.createXMLStreamWriter(out);
        //new IndentingXMLStreamWriter(xof.createXMLStreamWriter(out));

        // Write datacite md record to string
        toDataCite(item, doi, writer);

        log.debug("Submitting to datacite <{}>: \n{}\n", endpoint, out.toString());

        AbstractHttpEntity content = new StringEntity(out.toString(), StandardCharsets.UTF_8);
        content.setContentType("application/xml");

        HttpPost post = new HttpPost(endpoint);
        post.setEntity(content);

        return client.execute(post);
    }

    // Point DOI at url
    public HttpResponse submitDOI(CloseableHttpClient client, String endpoint,
            String doi, String url) throws IOException {
        // Trailing slash is a work around for a proxy issue
        String message = String.format("doi=%s\nurl=%s\n", doi, url + "/");

        log.debug("Submitting to datacite <{}>: \n{}\n", endpoint, message);

        AbstractHttpEntity content = new StringEntity(message, StandardCharsets.UTF_8);
        content.setContentType("text/plain");

        HttpPost post = new HttpPost(endpoint);
        post.setEntity(content);
        return client.execute(post);
    }

    // Write item out in datacite format
    public void toDataCite(Resource item, String doi, XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartDocument();

        // Preamble
        writer.setDefaultNamespace("http://datacite.org/schema/kernel-4");
        writer.writeStartElement("resource");
        writer.writeDefaultNamespace("http://datacite.org/schema/kernel-4");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4.0/metadata.xsd");

        // DOI
        writer.writeStartElement("identifier");
        writer.writeAttribute("identifierType", "DOI");
        writer.writeCharacters(doi);
        writer.writeEndElement();

        // Creators
        writer.writeStartElement("creators");
        writeNamed(item, DCTerms.creator, "creator", "creatorName", writer);
        writer.writeEndElement();

        // Titles
        writer.writeStartElement("titles");
        write(item, DCTerms.title, "title", writer);
        write(item, DCTerms.alternative, "title", writer, "titleType", "AlternativeTitle");
        writer.writeEndElement();

        // Publisher
        write(item, DCTerms.publisher, "publisher", writer);

        // Publication year
        write(item, DCTerms.issued, "publicationYear", writer);

        // Funders - complex because a) we changed how we did them, and
        // b) they are annoyingly different to other named things
        if (item.hasProperty(RepositoryVocab.funding)) {
            writer.writeStartElement("fundingReferences");
            StmtIterator allFunding = item.listProperties(RepositoryVocab.funding);
            while (allFunding.hasNext()) {
                Resource funding = allFunding.next().getObject().asResource();
                writer.writeStartElement("fundingReference");
                if (funding.hasProperty(RepositoryVocab.funder)) {
                    Resource funder = funding.getPropertyResourceValue(RepositoryVocab.funder);
                    write(funder, RDFS.label, "funderName", writer);
                    // If we have a uri from crossref mark it as such, otherwise 'other'
                    if (funder.isURIResource()) {
                        writer.writeStartElement("funderIdentifier");
                        String uri = funder.asResource().getURI();
                        if (uri.startsWith("http://dx.doi.org/10.13039/")) {
                            writer.writeAttribute("funderIdentifierType", "Crossref Funder ID");
                        } else {
                            writer.writeAttribute("funderIdentifierType", "Other");
                        }
                        writer.writeCharacters(uri);
                        writer.writeEndElement();
                    }
                }
                write(funding, Schema.identifier, "awardNumber", writer);
                writer.writeEndElement();
            }
            writer.writeEndElement();
        }

        // Dates
        if (item.hasProperty(DCTerms.valid) || item.hasProperty(DCTerms.created)) {
            writer.writeStartElement("dates");
            write(item, DCTerms.valid, "date", writer, "dateType", "Valid");
            write(item, DCTerms.created, "date", writer, "dateType", "Created");
            writer.writeEndElement();
        }

        // Subjects
        writeContained("subjects", item, DCTerms.subject, "subject", writer);

        // Resource type - always Dataset currently
        writer.writeEmptyElement("resourceType");
        writer.writeAttribute("resourceTypeGeneral", "Dataset");

        // Contributors
        writer.writeStartElement("contributors");

        // Researchers
        writeNamed(item, DCTerms.contributor,
                "contributor", "contributorName", writer, "contributorType", "Researcher");

        // End contributors
        writer.writeEndElement();

        // Language
        write(item, DCTerms.language, "language", writer);

        // Identifiers
        writeContained("alternateIdentifiers", item, DCTerms.identifier, "alternateIdentifier", writer, "alternateIdentifierType", "");

        // Related publications
        if (item.hasProperty(DCTerms.references) || item.hasProperty(DCTerms.isReferencedBy)) {
            writer.writeStartElement("relatedIdentifiers");
            write(item, DCTerms.references, "relatedIdentifier", writer,
                    "relationType", "Cites", "relatedIdentifierType", "URN");
            write(item, DCTerms.isReferencedBy, "relatedIdentifier", writer,
                    "relationType", "IsCitedBy", "relatedIdentifierType", "URN");
            writer.writeEndElement();
        }

        // Rights
        writeContained("rightsList", item, DCTerms.rights, "rights", writer);

        // Description
        writeContained("descriptions", item, DCTerms.description, "description", writer, "descriptionType", "Abstract");

        // Formats -- harder to extract since they are nested
        // We query for them
        writer.writeStartElement("formats");
        ResultSet allFormats = Util.query(item.getModel(), getFormats);
        while (allFormats.hasNext()) {
            RDFNode format = allFormats.next().get("format");
            writer.writeStartElement("format");
            writer.writeCharacters(toString(format));
            writer.writeEndElement();
        }
        writer.writeEndElement();

        // Close root and document
        writer.writeEndElement();
        writer.writeEndDocument();

        writer.flush();
    }

    // Wrapper for write which will include a containing element if there's anything
    // to write
    private void writeContained(String container,
            Resource item, Property property, String element,
            XMLStreamWriter writer, String... attVals) throws XMLStreamException {
        if (item.hasProperty(property)) {
            writer.writeStartElement(container);
            write(item, property, element, writer, attVals);
            writer.writeEndElement();
        }
    }

    // As above, but for writeNamed, i.e. composite id + name
    private void writeNamedContained(String container,
            Resource item, Property property, String containerElem, String nameElem,
            XMLStreamWriter writer, String... attVals) throws XMLStreamException {
        if (item.hasProperty(property)) {
            writer.writeStartElement(container);
            writeNamed(item, property, containerElem, nameElem, writer, attVals);
            writer.writeEndElement();
        }
    }

    // Checked exceptions and lambdas - unpleasant
    private interface NodeWriter {

        void apply(RDFNode node) throws XMLStreamException;
    }

    /**
     * Take an iterator, and for each object apply a function, unless the object
     * is a list, in which case apply to each item of the list
     *
     * Sort of a 'flatten'.
     *
     * @param si
     * @param withEach
     */
    private void eachItem(StmtIterator si, NodeWriter withEach) throws XMLStreamException {
        while (si.hasNext()) {
            RDFNode nextObject = si.next().getObject();

            // If the next object is a list apply for each item in list
            if (nextObject.canAs(RDFList.class)) {
                RDFList list = nextObject.as(RDFList.class);
                ExtendedIterator<RDFNode> it = list.iterator();
                while (it.hasNext()) {
                    withEach.apply(it.next());
                }
            } else {
                withEach.apply(nextObject);
            }
        }

        si.close();
    }

    /**
     * Write out rdf values, if present, to XML stream
     *
     * @param item Resource being written
     * @param property Property to write
     * @param element XML element to use
     * @param writer XML stream to write to
     * @param attVals Addition attribute values to include
     */
    private void write(Resource item, Property property, String element,
            XMLStreamWriter writer, String... attVals) throws XMLStreamException {

        eachItem(item.listProperties(property), thing -> {
            writer.writeStartElement(element);
            // Write attributes out
            for (int i = 0; i < attVals.length; i += 2) {
                writer.writeAttribute(attVals[i], attVals[i + 1]);
            }

            // Write value
            writer.writeCharacters(toString(thing));

            writer.writeEndElement();
        });

    }

    /**
     * Write out a composite entity -- name + identifier identifier element is
     * always 'nameIdentifier'
     *
     * For creators and contributors currently
     *
     * @param item
     * @param property
     * @param containerElem
     * @param nameElem
     * @param writer
     * @throws XMLStreamException
     */
    private void writeNamed(Resource item, Property property, String containerElem,
            String nameElem, XMLStreamWriter writer, String... attVals) throws XMLStreamException {

        eachItem(item.listProperties(property), thing -> {
            writer.writeStartElement(containerElem);

            // Write attributes out
            for (int i = 0; i < attVals.length; i += 2) {
                writer.writeAttribute(attVals[i], attVals[i + 1]);
            }

            if (thing.isLiteral()) { // Literal values only have names, no ids
                writer.writeStartElement(nameElem);
                writer.writeCharacters(thing.asLiteral().getLexicalForm());
                writer.writeEndElement();
            } else { // Name (from common name props) and possibly id
                Resource namedThing = thing.asResource();

                // Find suitable name and write out
                if (namedThing.hasProperty(FOAF.name)
                        || namedThing.hasProperty(DCTerms.title)
                        || namedThing.hasProperty(RDFS.label)) {

                    writer.writeStartElement(nameElem);

                    Statement val;
                    if (namedThing.hasProperty(FOAF.name)) {
                        val = namedThing.getProperty(FOAF.name);
                    } else if (namedThing.hasProperty(DCTerms.title)) {
                        val = namedThing.getProperty(DCTerms.title);
                    } else {
                        val = namedThing.getProperty(RDFS.label);
                    }

                    writer.writeCharacters(toString(val.getObject()));

                    writer.writeEndElement();
                }

                // Provide identifier if available
                if (namedThing.isURIResource()) {
                    writer.writeStartElement("nameIdentifier");
                    writer.writeAttribute("nameIdentifierScheme", "URN");
                    writer.writeCharacters(namedThing.getURI());
                    writer.writeEndElement();
                }
            }

            writer.writeEndElement();
        });

    }

    private String toString(RDFNode object) {
        if (object.isURIResource()) {
            return object.asResource().getURI();
        } else if (object.isLiteral()) {
            return object.asLiteral().getLexicalForm();
        } else {
            return "";
        }
    }

    public static void main(String[] args) throws JobExecutionException, IOException, XMLStreamException {

        Properties p = new Properties();
        p.load(DataCiteSubmit.class.getResourceAsStream("/sensitive.properties"));

        String user = p.getProperty("datacite.username");
        String pass = p.getProperty("datacite.password");

        if (null == user || null == pass) {
            System.err.printf("Missing username and / or password (%s,%s)\n",
                    user, pass);
            System.exit(1);
        }

        DataCiteSubmit instance = new DataCiteSubmit();

        Resource item = FileManager.get().loadModel("/home/pldms/Development/Projects/2013/data.bris/dundry/src/test/resources/datacite/data.ttl").
                getResource("http://example.com/repo/1zqd7egla2qu1x3q5ky6zz6c3");

        item.getModel().write(System.out, "TTL");

        StringWriter out = new StringWriter();
        XMLStreamWriter writer
                = //xof.createXMLStreamWriter(System.out);
                new IndentingXMLStreamWriter(xof.createXMLStreamWriter(System.out));

        // Write datacite md record to string
        instance.toDataCite(item, "10.5072/bris.ABCD", writer);

        //instance.submit(user, pass, "https://test.datacite.org/mds/", "10.5072/bris.", false,
        //        "16o6ls8w6l0md1oufmorwt8tbj", "http://data-bris.acrc.bris.ac.uk/datasets/16o6ls8w6l0md1oufmorwt8tbj/", item, item);
    }
}
