package uk.ac.bristol.dundry.tasks;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Task to copy from one directory to another. That's it.
 *
 * @author pldms
 */
public class DeleteTask implements Job {

    final static Logger log = LoggerFactory.getLogger(DeleteTask.class);
    public final static String TARGET = "delete-task-target";

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        JobDataMap jobData = jec.getMergedJobDataMap();
        String target = jobData.getString(TARGET);
        try {
            deletePath(Paths.get(target));
        } catch (IOException ex) {
            throw new JobExecutionException("Delete failed", ex);
        }
    }

    /**
     * Recursively delete an item
     *
     * @param target Item to delete 
     * @return
     * @throws IOException
     */
    protected Path deletePath(final Path target) throws IOException {

        log.debug("Delete {}", target);
        return Files.walkFileTree(target, new SimpleFileVisitor<Path>() {
            
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException ioex) throws IOException {
                if (ioex != null) throw ioex; // pass it on...
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
