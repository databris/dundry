package uk.ac.bristol.dundry.tasks;

import com.google.common.collect.ImmutableMap;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.vocabs.Bibo;
import uk.ac.bristol.dundry.webresources.providers.RdfMediaType;

/**
 * Create files for web publishing: header and footer for apache index, plus
 * write out metadata
 *
 * @author pldms
 */
public class GenerateApacheIndexes extends JobBase {

    final static Logger log = LoggerFactory.getLogger(GenerateApacheIndexes.class);

    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        // Fetch settings
        String templateBase = jobData.getString("indexpublish.templatebase");
        String headerTemplate = jobData.getString("indexpublish.headertemplate");
        String footerTemplate = jobData.getString("indexpublish.footertemplate");
        String headerFilename = jobData.getString("indexpublish.headerfilename");
        String footerFilename = jobData.getString("indexpublish.footerfilename");
        String rdfFilename = jobData.getString("indexpublish.rdffilename");
        String jsonFilename = jobData.getString("indexpublish.jsonfilename");

        log.info("Generate index for publishing: '{}' <{}> <{}> <{}> <{}> <{}> <{}>",
                new String[]{id, templateBase, headerTemplate, footerTemplate, headerFilename, footerFilename, rdfFilename});

        // Get all item metadata
        Resource metadata = repo.getFullMetadata(id);

        // Make a freemarker configuration
        Configuration conf = new Configuration();
        conf.setClassForTemplateLoading(GenerateApacheIndexes.class, templateBase);
        conf.setObjectWrapper(new DefaultObjectWrapper());

        // Our model for rendering
        Map<String, String> model = ImmutableMap.of(
                "title", getValue(metadata, DCTerms.title),
                "description", getValue(metadata, DCTerms.description),
                "doi", getValue(metadata, Bibo.doi));

        // Write header and footer
        write(model, conf, headerTemplate, root.resolve(headerFilename));
        write(model, conf, footerTemplate, root.resolve(footerFilename));

        // Link header and footer into all sub directories
        linkSubtree(root.resolve(headerFilename), root);
        linkSubtree(root.resolve(footerFilename), root);

        try {
            // Write rdf out
            repo.rdfSerialiser.writeMetadata(metadata, RdfMediaType.APPLICATION_RDF_XML_TYPE, rdfFilename, root);
            repo.rdfSerialiser.writeMetadata(metadata, RdfMediaType.APPLICATION_JSON_LD_TYPE, jsonFilename, root);
        } catch (IOException ex) {
            log.info("Issue writing metadata file out", ex);
            throw new JobExecutionException(ex);
        }
        
    }
        
    // Safe get string value
    private String getValue(Resource r, Property p) {
        if (r.hasProperty(p)) {
            return r.getProperty(p).getString();
        } else {
            return "";
        }
    }

    private void write(Map<String, String> model, Configuration conf, String templateName, Path target) throws JobExecutionException {
        log.debug("Write template {} with model {} to {}", new Object[]{templateName, model, target});

        try (Writer writer = Files.newBufferedWriter(target, StandardCharsets.UTF_8,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)) {
            Template template = conf.getTemplate(templateName);
            template.process(model, writer);
            writer.flush();
        } catch (TemplateException | IOException ex) {
            log.error("Problem executing template {}: {}", templateName, ex);
            throw new JobExecutionException("Error running template", ex);
        }
    }
    
    // Make links to target from all subdirs of root
    // For populating apache index
    private Path linkSubtree(final Path target, final Path root) throws JobExecutionException {
        try {
            return Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    if (!root.equals(dir)) {
                        // same file name as target, but in dir
                        Path link = dir.resolve(target.getFileName());
                        // relative path to target from dir
                        Path backToTarget = dir.relativize(target);
                        log.debug("Link <{}> to <{}>", link, backToTarget);
                        
                        Files.deleteIfExists(link);
                        
                        Files.createSymbolicLink(
                            link, 
                            backToTarget);
                    }
                    
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ex) {
            throw new JobExecutionException("Problem linking apache index", ex);
        }
    }
}
