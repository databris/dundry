package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.DCTypes;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayDeque;
import java.util.Deque;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.StmtIterator;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * Go through a new deposit, pulling the structure into the metadata
 *
 * @author pldms
 */
public class IndexDepositTask extends JobBase {

    final static Logger log = LoggerFactory.getLogger(IndexDepositTask.class);

    final static Path EMPTY = Paths.get("");

    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov,  Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData)
            throws JobExecutionException {
        try {
            log.debug("Indexing directory: <{}>", root);
            
            Path dataPath = repo.getDataPathForId(id);
            
            Model current = repo.getProvenanceMetadata(id).getModel();
            
            // Set up our indexer
            FileIndexer indexer = new FileIndexer(dataPath, id, prov, toRemoveFromProv, current);
            
            // And off it goes
            Files.walkFileTree(dataPath, indexer);
            
            // Record total size
            // Remove existing in case we're reindexing
            toRemoveFromProv.add(current.listStatements(prov, DCTerms.extent, (RDFNode) null));
            toRemoveFromProv.remove(prov, DCTerms.extent, ResourceFactory.createTypedLiteral(indexer.totalSize));
            prov.addLiteral(DCTerms.extent, indexer.totalSize);
            
        } catch (IOException ex) {
            throw new JobExecutionException("Index walk failed", ex);
        }
    }

    public static void main(String[] args) throws JobExecutionException {
        JobBase job = new IndexDepositTask();
        String id = "123456789";
        Resource prov = ModelFactory.createDefaultModel().createResource("http://example.com/repo/" + id);
        Resource item = ModelFactory.createDefaultModel().createResource("http://example.com/repo/" + id);
        Model toRemove =  ModelFactory.createDefaultModel();
        
        long start = System.nanoTime();
        job.execute(null, null, item, prov, toRemove, id, Paths.get("/Users/pldms/Pictures/Aperture Library.aplibrary/"), null);
        long tookMS = (System.nanoTime() - start) / 1_000_000;

        System.out.println("========== prov =========");
        prov.getModel().write(System.out, "TTL");
        System.out.println("========== item =========");
        item.getModel().write(System.out, "TTL");

        System.out.printf("Took %d ms\n", tookMS);
    }

    private static class FileIndexer extends SimpleFileVisitor<Path> {

        private final Path base;
        private final String id;
        private final Resource root;
        private long totalSize = 0;
        private final Model toRemove;
        private final Model current;

        public FileIndexer(Path base, String id, Resource root, Model toRemove, Model current) {
            this.base = base;
            this.id = id;
            
            this.root = root;
            this.toRemove = toRemove;
            this.current = current;
        }
        // Keep track of parents as we walk the tree, so
        // tree structure is recorded faithfully
        Deque<Resource> parents = new ArrayDeque<>();

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Path rel = base.relativize(file);
            log.trace("Visit {}", file);

            String filename = file.getFileName().toString();

            // Skip files that are artifacts of publishing, rather than data
            // Either a few well know (invisible) files, or any file
            // that includes the deposit id.
            if (Util.isIgnorableFile(file, id)) {
                log.trace("Ignoring {}", rel);
            } else {
                recordVisit(file, rel, RDFS.Resource, attrs); // we really don't know what it is
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            Path rel = base.relativize(dir);

            // Don't type the root -- it is set elsewhere
            Resource type = (EMPTY.equals(rel)) ? RDFS.Resource : DCTypes.Collection;

            // Record dir as a collection of resources
            Resource item = recordVisit(dir, rel, type, attrs);
            parents.push(item); // make this the current parent
            
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exception) {
            parents.pop(); // finished. Forget this directory

            return FileVisitResult.CONTINUE;
        }

        /**
         * Make a note of this item in the database
         *
         * @return item in database
         */
        private Resource recordVisit(Path absPath, Path relativePath, Resource type, 
                BasicFileAttributes attrs) throws IOException {
            
            Resource item = Util.append(root, relativePath);
                        
            if (!RDFS.Resource.equals(type)) {
                item.addProperty(RDF.type, type);
            }

            // Record file name
            if (!EMPTY.equals(relativePath)) {
                item.addProperty(RDFS.label, relativePath.getFileName().toString());
            }

            // If we have a parent link parent to child
            if (!parents.isEmpty()) {
                parents.peek().addProperty(DCTerms.hasPart, item);
            }

            // Record file size and hash of content
            if (Files.isRegularFile(absPath, LinkOption.NOFOLLOW_LINKS)) {
                
                totalSize += attrs.size();
                
                // Remove existing extent and hash, in case we are reindexing
                toRemove.add(current.listStatements(item, DCTerms.extent, (RDFNode) null));
                toRemove.add(current.listStatements(item, FOAF.sha1, (RDFNode) null));
                
                toRemove.remove(item, DCTerms.extent, ResourceFactory.createTypedLiteral(attrs.size()));
                item.addLiteral(DCTerms.extent, attrs.size());
                
                try {
                    String sha1 = Util.hexDigest(absPath, "sha1");
                    toRemove.remove(item, FOAF.sha1, ResourceFactory.createPlainLiteral(sha1));
                    item.addProperty(FOAF.sha1, Util.hexDigest(absPath, "sha1"));
                } catch (NoSuchAlgorithmException ex) {
                    log.error("Digest algorithm not known", ex);
                }
            }

            return item;
        }
    }
}
