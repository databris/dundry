package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.jena.rdf.model.Model;
import org.apache.tika.Tika;
import org.apache.tika.metadata.DublinCore;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class MDExtractTika extends JobBase {
    
    static final org.slf4j.Logger log = LoggerFactory.getLogger(MDExtractTika.class);
    
    static final Map<String, Property> FIELD_MAPPING =
            new HashMap<String, Property>() {{
                put(HttpHeaders.CONTENT_TYPE, DCTerms.format);
                put(DublinCore.TITLE.getName(), DCTerms.title);
                put(DublinCore.MODIFIED.getName(), DCTerms.modified);
                put(DublinCore.DATE.getName(), DCTerms.date);
                put(HttpHeaders.CONTENT_LENGTH, DCTerms.extent);
            }};

    
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) {
        try {
            log.debug("Extract metadata from <{}>", root);
            Files.walkFileTree(root, new MDExtractVisitor(root, prov, id));
        } catch (IOException ex) {
            log.error("Error extracting metadata under {}: {}", root, ex);
        }
    }
    
    static class MDExtractVisitor extends SimpleFileVisitor<Path> {
        private final Resource prov;
        private final Path root;
        private final Tika tika;
        private final String id;

        MDExtractVisitor(Path root, Resource prov, String id) {
            this.root = root;
            this.prov = prov;
            this.id = id;
            this.tika = new Tika();
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Path rel = root.relativize(file);
            
            // Skip ignorable files
            if (Util.isIgnorableFile(file, id)) {
                    log.trace("Ignoring {}", rel);
                    return FileVisitResult.CONTINUE;
            }
            
            Metadata metadata = new Metadata();
            
            // We don't bother with the parsed text, just want the metadata
            try (InputStream in = Files.newInputStream(file);
                    Reader reader = tika.parse(in, metadata)) {
                
                log.debug("Metadata for {}: {}", file, metadata);
                storeMetadata(metadata, rel, prov);
            }

            return FileVisitResult.CONTINUE;
        }

        private void storeMetadata(Metadata metadata, Path rel, Resource item) {
            Resource subject = Util.append(item, rel);
            
            for (Entry<String, Property> map: FIELD_MAPPING.entrySet()) {
                String[] values = metadata.getValues(map.getKey());
                if (values != null && values.length > 0) {
                    for (String value: values) subject.addProperty(map.getValue(), value);
                }
            }
        }
    }
    
    public static void main(String[] args) throws JobExecutionException {
        JobBase job = new MDExtractTika();
        String id = "123456789";
        Resource prov = ModelFactory.createDefaultModel().createResource("http://example.com/repo/" + id);
        Resource item = ModelFactory.createDefaultModel().createResource("http://example.com/repo/" + id);
        Model toRemoveFromProv = ModelFactory.createDefaultModel();
        
        job.execute(null, null, item, prov, toRemoveFromProv, id, Paths.get("/home/pldms/Development/Projects/2012/data.bris/dundry/working/RDSF_MV/nfs3-exports/marfc-cregan-2011/ACRC_Test_Area/2011/"), null);
        
        System.out.println("========== prov =========");
        prov.getModel().write(System.out, "TTL");
        System.out.println("========== item =========");
        item.getModel().write(System.out, "TTL");
    }
}
