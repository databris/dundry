package uk.ac.bristol.dundry.tasks;

import com.google.common.collect.Lists;
import java.nio.file.Path;
import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import java.util.List;
import org.apache.jena.rdf.model.Model;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 *
 * @author pldms
 */
public class PendingPublicationChecker extends JobBase {
    
    private static final Logger log = LoggerFactory.getLogger(PendingPublicationChecker.class);
    
    private static final Query FIND_PENDING_DUE = Util.loadQuery("queries/find_pending_due.rq");
    
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        
        log.info("Check pending...");
        
        List<String> due = findPublishPending(repo);
        
        log.info("{} pending items due for publish", due.size());
        
        for (String pubId: due) {
            log.info("Pending publication now due: {}", id);
            try {
                repo.runPublish(pubId);
            } catch (SchedulerException ex) {
                log.error("Error publishing pending item " + id, ex);
            }
        }
    }
    
    public List<String> findPublishPending(Repository repo) {
        List<RDFNode> items = repo.find(FIND_PENDING_DUE);
        
        List<String> idsToPublish = Lists.newArrayListWithCapacity(items.size());
        
        for (RDFNode item: items) {
            if (item instanceof Resource) {
                String id = repo.toExternalId(((Resource) item).getURI());
                idsToPublish.add(id);
            } else {
                // unlikely, but report
                log.warn("Pending publication query found literal: '{}'", item);
            }
        }
        
        return idsToPublish;
    }
}
