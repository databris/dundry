package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * Sym link root under a location for preview
 * @author pldms
 */
public class PreviewSymLinkTask extends JobBase {
    
    final static Logger log = LoggerFactory.getLogger(PreviewSymLinkTask.class);
        
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        String base = jobData.getString("preview.base");
        try {
            // Link root under publish base
            Util.symlink(Paths.get(base), root);
        } catch (IOException ex) {
            throw new JobExecutionException("Error symlinking for preview", ex);
        }
    }
    
}
