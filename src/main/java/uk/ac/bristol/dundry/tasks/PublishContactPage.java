package uk.ac.bristol.dundry.tasks;

import com.google.common.collect.Lists;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.webresources.providers.RdfMediaType;

/**
 * A special task for non-published publications Make a directory and put head
 * metadata only in there
 *
 * @author pldms
 */
public class PublishContactPage extends JobBase {

    final static Logger log = LoggerFactory.getLogger(JobBase.class);

    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {

        String rdfFilename = jobData.getString("indexpublish.rdffilename");
        String jsonFilename = jobData.getString("indexpublish.jsonfilename");
        
        // Be explicit here -- we want this, not the restricted data link
        Path publishPath = repo.getPublishPathForId(id);
        
        // Create it
        try {
            if (Files.notExists(publishPath)) {
                log.info("Creating public directory for restricted data");
                Files.createDirectory(publishPath);
            }
        } catch (IOException e) {
            throw new JobExecutionException("Problem creating publish directory " 
                    + publishPath.toString(), e);
        }

        // Get item metadata
        Resource metadata = repo.getFullMetadata(id);
        
        // SELECT HEAD INFO ONLY
        Resource headData = copyReachable(metadata);
        
        // Write rdf out
        try {
            // Write rdf out
            repo.rdfSerialiser.writeMetadata(headData, RdfMediaType.APPLICATION_RDF_XML_TYPE, rdfFilename, publishPath);
            repo.rdfSerialiser.writeMetadata(headData, RdfMediaType.APPLICATION_JSON_LD_TYPE, jsonFilename, publishPath);
        } catch (IOException ex) {
            log.info("Issue writing metadata file out", ex);
            throw new JobExecutionException(ex);
        }

    }
    
    /**
     * 
     * @param item
     * @return 
     */
    public Resource copyReachable(Resource item) {
        
        Model copied = ModelFactory.createDefaultModel();
        
        List<Resource> items = Collections.singletonList(item);
                
        while (!items.isEmpty()) {
            List<Resource> nextItems = Lists.newArrayListWithExpectedSize(items.size() * 2);
                        
            for (Resource currentItem: items) {
                                
                StmtIterator allProps = currentItem.listProperties();
        
                while (allProps.hasNext()) {
                    Statement s = allProps.nextStatement();
            
                    // Don't follow dc:hasPart
                    if (DCTerms.hasPart.equals(s.getPredicate())) {
                        continue;
                    }
                    
                    // Copy over
                    copied.add(s);
            
                    if (s.getObject().isResource()) {
                        nextItems.add(s.getObject().asResource());
                    }
                }
            }
            
            items = nextItems; // And off we go again
        }
        
        return copied.asRDFNode(item.asNode()).asResource();
    }

}
