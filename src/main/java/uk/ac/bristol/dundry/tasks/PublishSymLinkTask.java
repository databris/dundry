package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.vocabs.RepositoryVocab;

/**
 * Sym link root under a location for publication
 *
 * @author pldms
 */
public class PublishSymLinkTask extends JobBase {

    final static Logger log = LoggerFactory.getLogger(PublishSymLinkTask.class);
    
    /**
     * Link publish dir (may or may not contain data) in public area, and
     * data dir under link area for convenience
     * 
     * @param repo
     * @param fileSource
     * @param item
     * @param prov
     * @param id
     * @param root
     * @param jobData
     * @throws JobExecutionException 
     */
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        String publishBase = jobData.getString("publish.base");
        String linkBase = jobData.getString("link.base");

        // Be explicit here -- we want this, not the restricted data link
        Path publishPath = repo.getPublishPathForId(id);
        Path dataPath = repo.getDataPathForId(id);

        Path linkDir;
        
        Resource itemInfo = repo.getFullMetadata(id);
        
        // Where will we link?
        if (itemInfo.hasProperty(DCTerms.type, RepositoryVocab.RestrictedDataset)) {
            linkDir = Paths.get(linkBase, "Restricted");
        } else if (itemInfo.hasProperty(DCTerms.type, RepositoryVocab.ControlledDataset)) {
            linkDir = Paths.get(linkBase, "Controlled");
        } else if (itemInfo.hasProperty(DCTerms.type, RepositoryVocab.ClosedDataset)) {
            linkDir = Paths.get(linkBase, "Closed");
        } else {
            linkDir = Paths.get(linkBase, "Open");
        }
        
        try {
            
            // Ensure link dir in place
            if (!Files.exists(linkDir)) {
                log.info("Creating link directory {}.", linkDir);

                Files.createDirectories(linkDir);
            }
            
            // Link publish dir under publish base
            Util.symlink(Paths.get(publishBase), publishPath);
            
            // Link data under link base
            Util.symlink(linkDir, dataPath);
            
        } catch (IOException ex) {
            throw new JobExecutionException("Error symlinking for publish", ex);
        }
    }

}
