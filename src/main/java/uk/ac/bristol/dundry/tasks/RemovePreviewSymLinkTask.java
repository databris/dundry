package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * Remove sym link used for preview
 * @author pldms
 */
public class RemovePreviewSymLinkTask extends JobBase {
    
    final static Logger log = LoggerFactory.getLogger(RemovePreviewSymLinkTask.class);
        
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        String base = jobData.getString("preview.base");
        try {
            // Link is root filename under preview base
            Path link = Paths.get(base).resolve(root.getFileName());
            Files.delete(link);
        } catch (IOException ex) {
            throw new JobExecutionException("Error removing link for preview", ex);
        }
    }
    
}
