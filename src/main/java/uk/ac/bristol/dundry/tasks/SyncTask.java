package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.nio.file.*;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * Task to ensure contents of deposit are the same as source.
 * 
 * @author pldms
 */
public class SyncTask extends JobBase {
    
    final static Logger log = LoggerFactory.getLogger(SyncTask.class);

    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {
        
        Resource fullItem = repo.getFullMetadata(id);
        
        if (!fullItem.hasProperty(DCTerms.source)) {
            log.warn("Deposit {} has no source (possibly old deposit). Not syncing.", id);
            return;
        }
        
        // Get current source
        String from = fullItem.getRequiredProperty(DCTerms.source).getString();
        
        // Get filesource to work out actual path. then absolute-ize
        String realFrom = fileSource.getPath(from).toAbsolutePath().toString();
        
        // Ensure we sync _content_ not the dir itself
        if (!realFrom.endsWith("/")) {
            realFrom = realFrom + "/";
        }
        
        // Get deposit folder
        String to = repo.getDataPathForId(id).toAbsolutePath().toString();
        
        // sync, deleting missing items from target, ignoring invisible files
        Command.run(Paths.get("."), 
                "rsync", "-a", "--delete-excluded", "--exclude", ".*",
                "--exclude", "RDSF-Data-Bris-Folder-details.txt",
                realFrom, to);
    }
        
}
