package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.jena.rdf.model.Model;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * First part of zipping: decide whether zip in needed (based on size), and
 * if so note that in metadata.
 * 
 * Why? Well zip, or .zipinfo, also contain the metadata so we had a bootstrapping
 * issue. Separating into two steps avoids that pain.
 * 
 * @author pldms
 */
public class ZipPreTask extends JobBase {
    
    final static Logger log = LoggerFactory.getLogger(ZipPreTask.class);
    
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {        
        
        long limit = jobData.getLong("zip.maxsize");
        
        boolean withinLimit;
        try {
            withinLimit = directorySmallerThan(root, limit);
        } catch (IOException ex) {
            throw new JobExecutionException("Problem getting size of deposit", ex);
        }
        
        // If directory is below size limit
        if (withinLimit) {
            log.info("{} within zip limit {}. Adding alternative format.", id, limit);
            Path zipFile = Paths.get(id + ".zip");            
            // Record alternative format for the deposit
            Resource zipRes = Util.append(prov, zipFile);
            prov.addProperty(DCTerms.hasFormat, zipRes);
            zipRes.addProperty(DCTerms.format, "application/zip");
        } else {
            log.info("{} exceeds zip limit {}. Zip will be dynamic.", id, limit);
        }
    }

    protected boolean directorySmallerThan(Path root, final long limit) throws IOException {
        // a mutable long
        final AtomicLong total = new AtomicLong(0);
        
        Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
                        
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) 
                    throws IOException {
                boolean finished;
                if (attrs.isRegularFile()) {
                    // Accumulate total size, and note whether we're over limit
                    finished = total.addAndGet(attrs.size()) >= limit;
                } else {
                    finished = false;
                }
                                
                // Fail fast if finished
                return finished ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
            }
        });
                
        return total.get() < limit;
    }
}
