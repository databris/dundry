package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.DC;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;

/**
 * Zip up directory or generate .zipinfo (dynamic case)
 * 
 * Which runs is determined by ZipPreTask
 * 
 * @author pldms
 */
public class ZipTask extends JobBase {
    
    final static Logger log = LoggerFactory.getLogger(ZipTask.class);
    
    @Override
    public void execute(Repository repo, FileSystemSource fileSource, 
            Resource item, Resource prov, Model toRemoveFromProv,
            String id, Path root, JobDataMap jobData) throws JobExecutionException {        
        
        String zipfile = getZipAlternative(repo.getProvenanceMetadata(id));
        
        // If there is an alternative
        if (zipfile != null) {
            log.info("{} has zip alternative format <{}>. Zipping...", id, zipfile);
            Path zipPath = root.resolve(zipfile);
            
            Command.run(
                    root.getParent(), // directory to run in
                    "zip", "-q", "-r",
                    zipPath.toAbsolutePath().toString(), // zip output
                    root.getFileName().toString() // zip this
                    );
        } else {
            // Prepare dynamic zip info
            log.info("{} has no zip alternative. Making .zipinfo", id);
            Command.run(
                    root,
                    "/usr/local/bin/zip_indexer.py",
                    root.toAbsolutePath().toString(),
                    "/static/publications/" + id + "/",
                    id + "/",
                    ".zipinfo"
            );
        }
    }
    
    public String getZipAlternative(Resource deposit) {
        // Ok, bit cautious this. SPARQL query would be more succinct
        // Find r: ?s dc:hasFormat ?r . ?r dc:format zip
        StmtIterator si = deposit.listProperties(DCTerms.hasFormat);
        while (si.hasNext()) {
            RDFNode r = si.next().getObject();
            if (r.isResource() && 
                    r.asResource().hasProperty(DCTerms.format, "application/zip")) {
                String zipURI = r.asResource().getURI();
                String depositURI = deposit.getURI();
                // Shoddy relative path:
                // https://example/foo
                // https://example/foo/bar -> bar
                return zipURI.substring(depositURI.length() + 1);
            }
        }
        return null;
    }
}
