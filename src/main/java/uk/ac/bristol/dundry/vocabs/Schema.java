package uk.ac.bristol.dundry.vocabs;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * No schemagen for this - too big
 * 
 * @author cmdms
 */
public class Schema {
    final static String NS = "http://schema.org/";
    
    final public static Property identifier = ResourceFactory.createProperty(NS, "identifier");
}
