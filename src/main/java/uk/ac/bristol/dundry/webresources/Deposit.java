package uk.ac.bristol.dundry.webresources;

import com.google.common.base.Joiner;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.vocabulary.RDFS;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.dao.Repository.State;
import uk.ac.bristol.dundry.model.ResourceCollection;
import uk.ac.bristol.dundry.model.Value;
import uk.ac.bristol.dundry.vocabs.RepositoryVocab;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@Component
@Path("deposits")
public class Deposit {
    
    final static Logger log = LoggerFactory.getLogger(Deposit.class);
    
    @Autowired Repository repository;
    @Autowired FileSystemSource sourceFS;
    
    @GET
    public Response list() {
        return Response.ok(repository.getIds()).build();
    }
    
    @POST
    @Consumes("application/json")
    public Response create(Model data) throws IOException, SchedulerException {
        
        // Work out subject
        if (!data.contains(null, DCTerms.title, (RDFNode) null)) {
            log.info("Failed creation: no title found");
            return Response.status(Status.BAD_REQUEST).entity("We expect at least a title").build();
        }
        
        Resource record = data.listSubjectsWithProperty(DCTerms.title).next();
        
        log.info("Create from record title: {} desc: {} base: {} depositor: {}", new Object[]{
            record.getProperty(DCTerms.title),
            record.getProperty(DCTerms.description),
            record.getProperty(RepositoryVocab.base_directory),
            record.getProperty(RepositoryVocab.depositor)
        });
                
        String id = repository.create(
                record.getRequiredProperty(RepositoryVocab.depositor).getString(),
                record);
        
        URI createdUri = URI.create(id);
        
        return Response.created(createdUri).build();
    }
    
    @Path("{item}")
    @GET
    public Response retrieve(@PathParam("item") String item, @QueryParam("format") @DefaultValue("short") String format) {
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        // long means all known data about item, from all stores
        Resource itemData = ("long".equals(format)) 
                ? repository.getFullMetadata(item)
                : repository.getMetadata(item);
        
        return Response.ok(itemData).build();
    }
    
    @Path("{item}/state")
    @GET
    public Response retrieveState(@PathParam("item") String item) {
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        return Response.ok(new Value(repository.getState(item))).build();
    }
    
    @Path("{item}")
    @PUT
    public Response update(@PathParam("item") String item, Model data) {
        if (log.isDebugEnabled()) {
            log.debug("Update: {} with {}", item, data);
        }
        
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        Resource record = data.createResource(repository.toInternalId(item));
        
        repository.updateMetadata(item, record);
        return Response.ok().build();
    }
    
    @Path("{item}/contents")
    @GET
    public Response getContents(@PathParam("item") String item) throws IOException {
        log.info("Get content in {}", item);
        
        // Check we have something to add to
        if (!repository.hasId(item)) return Response.status(Response.Status.NOT_FOUND).build();
        
        List<java.nio.file.Path> contents = repository.getDepositContents(item);
        
        final Model m = ModelFactory.createDefaultModel();
        
        // Return something useful, including file names
        List<Resource> pathsAsResources = contents.stream()
                .map(path -> { 
                    Resource r = Util.toResource(m, path);
                    r.addProperty(RDFS.label, path.getFileName().toString());
                    return r;})
                .collect(Collectors.toList());

        return Response.ok(new ResourceCollection(pathsAsResources)).build();
    }
    
    // Add source to deposit
    @Path("{item}/contents")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response setContent(@PathParam("item") String item, @FormParam("source") String source) throws SchedulerException {
        log.info("Set content from source <{}> to item <{}>", source, item);
        
        // No nulls please
        if (source == null) return Response.status(Status.BAD_REQUEST).entity("No source provided").build();
        
        // Check we have something to add to
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        repository.setSource(item, source);
        
        return Response.ok().build();
    }
    
    // Publish
    @Path("{item}/publish")
    @POST
    public Response publish(@PathParam("item") String item) throws SchedulerException {
        log.info("Publish item {}", item);
        
        // Check we have something to add to
        if (!repository.hasId(item)) return Response.status(Response.Status.NOT_FOUND).build();
        
        repository.publish(item);
        
        return Response.ok().build();
    }
    
    // Hide
    @Path("{item}/hide")
    @POST
    public Response hide(@PathParam("item") String item) throws IOException {
        log.warn("Hide item {}", item);
        
        // Check we have something to hide
        if (!repository.hasId(item)) return Response.status(Response.Status.NOT_FOUND).build();
        
        repository.hide(item);
        
        return Response.ok().entity("Hidden " + item).build();
    }
    
    @Path("{item}")
    @DELETE
    public Response delete(@PathParam("item") String item) {
        log.info("DELETE: {}", item);
        
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        repository.delete(item);
        
        return Response.ok().build();
    }
    
    @Path("{item}/run")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response runTasks(@PathParam("item") String item, @FormParam("class") List<String> classes) throws Exception {
        String classSummary = Joiner.on(" , ").join(classes);
        log.info("Run tasks: {} [{}]", item, classSummary);
        
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        repository.executeTasks(item, classes);
        
        return Response.ok("That all seems to have worked\n" + classSummary + "\n").build();
    }
    
    @Path("{item}/state")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response setState(@PathParam("item") String item, @FormParam("state") State state) throws Exception {
        log.info("Set state: {} [{}]", item, state);
        
        // Does item exist?
        if (!repository.hasId(item)) return Response.status(Status.NOT_FOUND).build();
        
        repository.setState(item, state);
        
        return Response.ok("State change seems to have worked\n" + repository.getState(item) + "\n").build();
    }
}
