package uk.ac.bristol.dundry.webresources;

import java.io.IOException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.dao.FileSystemSource;
import uk.ac.bristol.dundry.model.FileTree;

/**
 * REST Web Service
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@Component
@Path("sources")
public class Source {
    
    Logger log = LoggerFactory.getLogger(Source.class);
    
    @Context
    private UriInfo context;
    
    @Autowired
    private FileSystemSource fsLister;
    
    /**
     * Creates a new instance of Source
     */
    public Source() {
    }
    
    @Path("/{dir: .+}")
    @GET
    public Response listDir(
            @PathParam("dir") String path,
            @QueryParam("maxDepth") @DefaultValue("3") int maxDepth,
            @QueryParam("maxItems") @DefaultValue("5") int maxItems,
            @QueryParam("calcSize") @DefaultValue("false") boolean calcSize
    ) throws IOException {
        log.info("List source path: {}, maxDepth: {}, maxItems: {}", 
                new Object[] { path, maxDepth, maxItems } );
        
        if (path.contains("..")) // No reason to allow these, and dangerous
            return Response.status(Response.Status.BAD_REQUEST).
                    entity("We don't accept relative paths").build();
        
        long start = System.currentTimeMillis();
        FileTree listing = fsLister.getTreeAt(path, maxDepth, maxItems, calcSize);
        
        log.info("Listing: {}, took: {} ms", path, System.currentTimeMillis() - start);
        
        if (listing == FileSystemSource.NONE)
            return Response.status(Response.Status.NOT_FOUND).build(); 
        else
            return Response.ok(listing).build();
    }
}
