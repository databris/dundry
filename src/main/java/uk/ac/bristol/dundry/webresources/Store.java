package uk.ac.bristol.dundry.webresources;

import javax.ws.rs.Consumes;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.dao.MetadataStore;

/**
 * 
 * Direct access to RDF store.
 * 
 * Internal consumption only!
 * 
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@Component
@Path("store")
public class Store {
    
    final static Logger log = LoggerFactory.getLogger(Store.class);
    
    @Autowired MetadataStore mds;
    
    @GET
    @Path("dump")
    public Response dump() {
        log.info("Dump metadata store");
        return Response.ok(mds.dump()).build();
    }
    
    @GET
    public Response query(@QueryParam("query") String query) {
        log.info("Direct query on metadata store: \"{}\"", query);
        Query q = QueryFactory.create(query);
        return Response.ok(mds.query(q)).build();
    }
    
    @POST
    public Response updateForm(@FormParam("update") String update) {
        log.warn("Direct update (FORM) on metadata store: \"{}\"", update);
        mds.update(update);
        return Response.ok("Update succeeded:\n\"" + update + "\"").build();
    }
    
    @POST
    @Consumes("application/sparql-update")
    public Response updateBody(String update) {
        log.warn("Direct update (BODY) on metadata store: \"{}\"", update);
        mds.update(update);
        return Response.ok("Update succeeded:\n\"" + update + "\"").build();
    }
    
}
