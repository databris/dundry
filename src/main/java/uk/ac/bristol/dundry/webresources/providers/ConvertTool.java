package uk.ac.bristol.dundry.webresources.providers;

import com.github.jsonldjava.core.JsonLdError;
import java.io.IOException;
import java.util.Collections;
import javax.ws.rs.core.MediaType;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;

/**
 * A tool to convert between RDF formats (especially to JSON-LD)
 * 
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class ConvertTool {
    
    public static void main(String[] args) throws ParseException, JsonLdError, IOException {
                
        Option context = Option.builder("C")
                .required(false)
                .hasArg()
                .desc("JSON-LD context")
                .longOpt("context")
                .build();
        
        Option base = Option.builder("b")
                .required(false)
                .hasArg()
                .desc("base")
                .longOpt("base")
                .build();
        
        Option inFormat = Option.builder("I")
                .required(false)
                .hasArg()
                .desc("Input format")
                .longOpt("informat")
                .build();
        
        Option outFormat = Option.builder("O")
                .required(false)
                .hasArg()
                .desc("Output format")
                .longOpt("outformat")
                .build();
        
        Option in = Option.builder("i")
                .required(false)
                .hasArg()
                .desc("Input source")
                .longOpt("in")
                .build();
        
        Options options = new Options();
        options.addOption(context);
        options.addOption(base);
        options.addOption(inFormat);
        options.addOption(outFormat);
        options.addOption(in);
        
        CommandLine cl = (new DefaultParser()).parse(options, args);
        
        MediaType outType = getType(cl.getOptionValue("O"));
        
        String inSource = cl.hasOption("i")
                ? cl.getOptionValue("i")
                : "-";
        
        String baseURL = cl.hasOption("b")
                ? cl.getOptionValue("b")
                : "http://example.com/";
        
        String contextURL = cl.hasOption("C")
                ? cl.getOptionValue("C")
                : "http://schema.org/";
        
        RDFSerialiser serialiser = new RDFSerialiser(Collections.EMPTY_MAP, contextURL, baseURL);
        
        Model model = RDFDataMgr.loadModel(inSource);
        
        serialiser.writeResource(findRoot(model), System.out, outType);
    }

    private static MediaType getType(String optionValue) {
        return RdfMediaType.TYPE_LOOKUP.getOrDefault(optionValue, RdfMediaType.APPLICATION_RDF_XML_TYPE);
    }

    private static Resource findRoot(Model model) {
        ResIterator subjects = model.listSubjects();
        while (subjects.hasNext()) {
            Resource subject = subjects.next();
            
            // Not an object of anything
            if (!model.listStatements(null, null, subject).hasNext()) {
                return subject;
            }
        }
        
        throw new RuntimeException("No root found in model");
    }
    
}
