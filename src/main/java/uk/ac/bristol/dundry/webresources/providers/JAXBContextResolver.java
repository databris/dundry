package uk.ac.bristol.dundry.webresources.providers;

import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.json.JSONJAXBContext;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.model.FileTree;

/**
 * Use 'natural' json encoding, which keeps arrays as arrays
 * 
 * @author pldms
 */
@Component
@Provider
public class JAXBContextResolver implements ContextResolver<JAXBContext> {

    private final JAXBContext context;
    private final Class[] types = {FileTree.class};

    public JAXBContextResolver() throws Exception {
        this.context
                = new JSONJAXBContext(
        JSONConfiguration.natural().build(), types);
    }

    @Override
    public JAXBContext getContext(Class<?> objectType) {
        
        if (objectType == FileTree.class) {
            return context;
        }
        
        return null;
    }
}
