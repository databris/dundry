package uk.ac.bristol.dundry.webresources.providers;

import com.github.jsonldjava.core.Context;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.google.common.base.Strings;
import org.apache.jena.rdf.model.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * A custom JSONLD writer
 *
 * Take a root or list of roots, and produces JSONLD using a context.
 * 
 * Attempts to be reasonably idiomatic
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class JSONLDWriter {

    final static Logger log = LoggerFactory.getLogger(JSONLDWriter.class);
    
    private final String contextURL;
    private final String baseURL;
    private final int baseURLLength;
    private final Context context;
    final Map<String, String> listMap;
    private final JsonLdOptions opts;
    private final HashMap<String, Object> localCtx;

    /**
     * Use this context url in serialisation
     *
     * @param contextURL json-ld context URL
     */
    public JSONLDWriter(String contextURL, String baseURL) throws JsonLdError {

        this.contextURL = contextURL;
        this.baseURL = baseURL;
        this.baseURLLength = baseURL.length();
        
        Context context_ = new Context();
        context = context_.parse(contextURL);
        
        OutputStreamWriter o = new OutputStreamWriter(System.out, StandardCharsets.UTF_8);
        
        // Poke into context manually
        Map<String, Object> contextMap = 
                (Map<String, Object>) context.serialize().get("@context");
        
        // Take those mappings that are lists, and make a uri -> short version map for them
        listMap = contextMap.entrySet().stream()
                // Keep items of form short -> { @container -> @list }
                .filter(e -> getVal(e.getValue(), "@container").equals("@list"))
                // Make map val(@id) -> short
                .collect(Collectors.toMap(e -> getVal(e.getValue(), "@id"), Entry::getKey));
        
        // Remember these for serialisation - hope they aren't modified
        opts = new JsonLdOptions();
        opts.useNamespaces = true;
        opts.setUseNativeTypes(true);
        opts.setCompactArrays(true);
        opts.setEmbed(false);
        
        localCtx = new HashMap<String, Object>();
        localCtx.put("@context", contextURL);
    }
    
    // Given something that may be a string or a map<string,string>, find a key value
    private static String getVal(Object obj, String key) {
        if (obj instanceof String) return "@none";
        return ((Map<String, String>) obj).getOrDefault(key, "@none");
    }
    
    /**
     * Write out a single resource as JSON-LD using the provided context
     * 
     * @param r
     * @param out 
     */
    public void write(Resource r, OutputStream out) 
            throws IOException {
        
        // convert resource to 'fat' json-ld
        Map<String, Object> jsonObject = fromResource(r, new HashSet<>());
        
        prettyWriteJSON(compact(jsonObject), out);
    }
    
    /**
     * Write out multiple resources to JSON-LD using the provided context
     * 
     * @param rs
     * @param out
     * @param asLDObject If false write a single json-ld object. If true, an array of objects.
     * @throws IOException
     */
    public void write(Collection<Resource> rs, OutputStream out, boolean asLDObject) 
            throws IOException {
        
        Object jsonObject;
        
        if (asLDObject) {
            // In this case we want a single json-ld object containing a graph
            // with all the members of the collection
            
            // This will be the graph
            List<Object> jsonList = rs.stream()
                .map(r -> fromResource(r, new HashSet<>()))
                .collect(Collectors.toList());
            
            // Now add this as the graph of an object
            Map<String, Object> jo = new HashMap<>(3); // Avoid rehash, this is small
            jo.put("@graph", jsonList);
            
            // And compact the whole thing
            jsonObject = compact(jo);
        } else {
            // In this case it's simple: a list of json-ld objects
            
            jsonObject = rs.stream()
                .map(r -> compact(fromResource(r, new HashSet<>())))
                .collect(Collectors.toList());
        }
                
        prettyWriteJSON(jsonObject, out);
    }
    
    /**
     * Take a json-ld object, compact it using the provided context, and write it out
     * 
     * @param jsonObject
     */
    public Map<String, Object> compact(Map<String, Object> jsonObject) {
        
        Map<String, Object> json;
        
        try {
            json = JsonLdProcessor.compact(jsonObject, localCtx, opts);
        } catch (JsonLdError ex) {
            throw new RuntimeException("Problem compacting JSON-LD", ex);
        }
            
        // Work around issue with java library or spec where list names 
        // aren't shortened
        fixCompactNames(json);
            
        json.put("@context", contextURL);
        json.put("@base", baseURL);
        
        return json;
    }
    
    public void prettyWriteJSON(Object json, OutputStream out) throws IOException {
        try (Writer writer = new OutputStreamWriter(out, StandardCharsets.UTF_8)) {
            JsonUtils.writePrettyPrint(writer, json);
        }
    }
    
    /**
     * Take a resource, and return a json-ld object with resource as the root
     * 
     * @param s
     * @param seen Resources to ignore (to avoid looping)
     * @return 
     */
    private Map<String, Object> fromResource(Resource s, Set<Resource> seen) {
        
        Map<String, Object> object = new HashMap<>();
        
        // Object has URI - include it
        if (s.isURIResource()) {
            String uri = s.getURI();
            // relative-ise
            String rel_uri =  uri.startsWith(baseURL) ? 
                    uri.substring(baseURLLength) : 
                    uri ;
            object.put("@id", rel_uri);
        }
        
        // If this has already been seen we are done        
        if (seen.contains(s)) return object;
        
        // Clone seen
        // We care about loops, not repetition between branches
        seen = new HashSet<>(seen);        
        seen.add(s);
        
        StmtIterator props = s.listProperties();
        while (props.hasNext()) {
            Statement stmt = props.next();
            String p = stmt.getPredicate().getURI();
            RDFNode o = stmt.getObject();
            
            List<Object> vals;
            
            if (object.containsKey(p)) {
                vals = (List<Object>) object.get(p);
            } else {
                vals = new ArrayList<>();
                object.put(p, vals);
            }
            
            Object converted;
                        
            if (o.isLiteral()) {
                converted = fromLiteral(o.asLiteral());
            } else if (o.canAs(RDFList.class) && listMap.containsKey(p)) {
                // if this is an RDFList, and ought to be a list according to the context
                converted = fromList(o.as(RDFList.class), seen);
            } else {
                converted = fromResource(o.asResource(), seen);
            }
            
            vals.add(converted);
        }
        return object;
    }
    
    /**
     * Take a literal and return a json-ld literal
     * 
     * @param literal
     * @return 
     */
    private Object fromLiteral(Literal literal) {
        String lang = Strings.emptyToNull(literal.getLanguage());
        String dt = Strings.emptyToNull(literal.getDatatypeURI());
        
        // Ignore xsd:string
        if ("http://www.w3.org/2001/XMLSchema#string".equals(dt)) dt = null;
        
        // Simple case: plain literal
        if (lang == null && dt == null) return literal.getLexicalForm();
        
        // This will be pretty small
        Map<String, String> litObj = new HashMap<>(4);
        
        litObj.put("@value", literal.getLexicalForm());
        if (lang != null) litObj.put("@language", lang);
        if (dt != null) litObj.put("@type", dt);
        
        return litObj;
    }
    
    /**
     * Take an RDF list and return a json-ld array with mapped values
     * 
     * @param list
     * @param seen
     * @return 
     */
    private Object fromList(RDFList list, Set<Resource> seen) {
        // Convert each item
        // (Could be simpler, but we only convert RDFNode here)
        return list.mapWith(n -> n.isLiteral() ? 
                fromLiteral(n.asLiteral()) : 
                fromResource(n.asResource(), seen)
        ).toList();
    }
    
    /**
     * Work around an issue with json-ld compaction somewhere
     * 
     * Property names aren't being shortened in this case, so we do it manually
     * 
     * @param json 
     */
    private void fixCompactNames(Map<String, Object> json) {
                
        for (Entry<String, Object> e: new HashSet<Entry<String, Object>>(json.entrySet())) {
            
            // Get short version of key if available
            String replacement = listMap.get(e.getKey());
            
            // If there is one then fix json
            if (replacement != null) {
                json.remove(e.getKey()); // out with old mapping
                
                // this should be an array too
                if (e.getValue() instanceof List) {
                    json.put(replacement, e.getValue()); // in with new
                } else { // ensure replacement is an array
                    json.put(replacement, Collections.singletonList(e.getValue()));
                }
            }
            
            // If value is a map keep going
            if (e.getValue() instanceof Map) {
                fixCompactNames((Map<String, Object>) e.getValue());
            }
        }
    }
}
