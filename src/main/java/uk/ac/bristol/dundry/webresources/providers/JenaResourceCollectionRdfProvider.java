package uk.ac.bristol.dundry.webresources.providers;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.model.ResourceCollection;

/**
 * @author Mike Jones (mike.a.jones@bristol.ac.uk)
 * @version $Id: JenaResourceRdfProvider.java 177 2008-05-30 13:50:59Z mike.a.jones $
 */
@Component
@Provider
@Produces({RdfMediaType.TEXT_TURTLE, RdfMediaType.APPLICATION_RDF_XML,
    RdfMediaType.TEXT_RDF_N3,  RdfMediaType.APPLICATION_JSON_LD,
    MediaType.APPLICATION_JSON, MediaType.WILDCARD})
public final class JenaResourceCollectionRdfProvider implements MessageBodyWriter<ResourceCollection> {
    
    @Autowired
    RDFSerialiser serialiser;
    
    // ---- Writer implementation

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return ResourceCollection.class.isAssignableFrom(aClass);
    }

    @Override
    public long getSize(ResourceCollection o, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {

        // since we are dealing with a model we would need to serialize it to the desired
        // format before we would know the size.
        return -1;
    }

    @Override
    public void writeTo(final ResourceCollection resourceCollection, final Class<?> aClass, final Type type,
                        final Annotation[] annotations, final MediaType mediaType,
                        final MultivaluedMap<String, Object> stringObjectMultivaluedMap,
                        final OutputStream outputStream) throws IOException {

        serialiser.writeResources(resourceCollection, outputStream, mediaType);
    }

}
