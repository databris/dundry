package uk.ac.bristol.dundry.webresources.providers;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ResultSet;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Provider
@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, 
    MediaType.APPLICATION_JSON, RdfMediaType.TEXT_CSV, RdfMediaType.TEXT_TSV})
public final class JenaResultSetProvider 
    implements MessageBodyWriter<ResultSet> {
    
    @Autowired
    RDFSerialiser serialiser;
    
    @Override
    public boolean isWriteable(Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return ResultSet.class.isAssignableFrom(type);
    }
    
    @Override
    public long getSize(ResultSet o, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(final ResultSet results, final Class<?> aClass, final Type type,
                        final Annotation[] annotations, final MediaType mediaType,
                        final MultivaluedMap<String, Object> stringObjectMultivaluedMap,
                        final OutputStream outputStream) throws IOException,
            WebApplicationException {
        serialiser.writeResultSet(results, outputStream, mediaType);
    }
}
