package uk.ac.bristol.dundry.webresources.providers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.springframework.stereotype.Component;
import uk.ac.bristol.dundry.exceptions.NotFoundException;

/**
 *
 * @author cmdms
 */
@Provider
@Component
public class NotFoundExceptionMapper  implements
                ExceptionMapper<NotFoundException> {
    
    @Override
    public Response toResponse(NotFoundException e) {
        return Response.status(404).entity(e.getMessage())
            .type("text/plain").build();
    }
    
}
