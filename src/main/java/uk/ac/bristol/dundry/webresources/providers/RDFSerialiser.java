package uk.ac.bristol.dundry.webresources.providers;

import com.github.jsonldjava.core.JsonLdError;
import java.io.IOException;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.shared.impl.PrefixMappingImpl;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.bristol.dundry.model.ResourceCollection;

/**
 * A common class to handle this task for a couple of providers
 * 
 * Also includes default prefix mapping for pretty results
 * 
 * @author pldms
 */
public class RDFSerialiser {
    
    private static final Logger log = LoggerFactory.getLogger(RDFSerialiser.class);
    
    private final PrefixMapping prefixes;
    private final JSONLDWriter jsonldWriter;
    private final String base;
    
    public RDFSerialiser(Map<String, String> prefixToURI, 
            String context, String base) throws JsonLdError {
        this.prefixes = new PrefixMappingImpl();
        this.prefixes.setNsPrefixes(prefixToURI);
        
        this.jsonldWriter = new JSONLDWriter(context, base);
        this.base = base;
    }
    
    public void writeResource(Resource resource, OutputStream outputStream, MediaType mediaType) throws IOException {
        
        // Special case: we need to control this carefully
        if (mediaType.equals(MediaType.APPLICATION_JSON_TYPE) ||
                mediaType.equals(RdfMediaType.APPLICATION_JSON_LD_TYPE)) {
            jsonldWriter.write(resource, outputStream);
        } else {
            writeModel(resource.getModel(), outputStream, mediaType);
        }
    }

    void writeResources(ResourceCollection resourceCollection, 
            OutputStream outputStream, MediaType mediaType) throws IOException {
        // Special case: we need to control this carefully
        // vanilla JSON gets an array of json-ld, explicit request gets json-ld object
        if (mediaType.equals(MediaType.APPLICATION_JSON_TYPE)) {
            jsonldWriter.write(resourceCollection, outputStream, false);
        } else if (mediaType.equals(RdfMediaType.APPLICATION_JSON_LD_TYPE)) {
            jsonldWriter.write(resourceCollection, outputStream, true);
        } else {
            writeModel(resourceCollection.getModel(), outputStream, mediaType);
        }
    }
    
    public void writeModel(Model model, OutputStream outputStream, MediaType mediaType) {
        
        Lang lang = langFromMediaType(mediaType);
        
        // Default to turtle
        if (lang == null) lang = Lang.TURTLE;
        
        // These models ought to be ephemeral
        model.setNsPrefixes(prefixes);
                
        RDFDataMgr.write(outputStream, model, lang);
    }
    
    public void writeDataset(Dataset dataset, OutputStream outputStream, MediaType mediaType) {
        
        Lang lang = langFromMediaType(mediaType);
        
        // Default to NQUADS
        if (lang == null) lang = Lang.NQUADS;
        
        // Hmm, how to do this?
        //dataset.setNsPrefixes(prefixes);
        
        // We need to be careful, since no defensive copy occurs
        // for dataset. Get transactions right...
        dataset.begin(ReadWrite.READ);
        try {
            RDFDataMgr.write(outputStream, dataset, lang);
        } finally {
            dataset.end();
        }
    }
    
    public void writeResultSet(ResultSet results, OutputStream outputStream, MediaType mediaType) {
        
        if (MediaType.APPLICATION_JSON_TYPE.equals(mediaType)) {
            ResultSetFormatter.outputAsJSON(outputStream, results);
        } else if (MediaType.APPLICATION_XML_TYPE.equals(mediaType)) {
            ResultSetFormatter.outputAsXML(outputStream, results);
        } else if (RdfMediaType.TEXT_CSV_TYPE.equals(mediaType)) {
            ResultSetFormatter.outputAsCSV(outputStream, results);
        } else if (RdfMediaType.TEXT_TSV_TYPE.equals(mediaType)) {
            ResultSetFormatter.outputAsTSV(outputStream, results);
        } else {
            ResultSetFormatter.out(outputStream, results, prefixes);
        }
    }
    
    public PrefixMapping getDefaultPrefixes() {
        return prefixes;
    }
    
    public String getBase() {
        return base;
    }
    
    // Handle json as json-ld
    public Lang langFromMediaType(MediaType mediaType) {
        if (MediaType.APPLICATION_JSON_TYPE.equals(mediaType)) {
            return Lang.JSONLD;
        } else {
            return RDFLanguages.contentTypeToLang(mediaType.toString());
        }
    }

    /**
     * Write resource out to a file
     *
     * @param md Resource to write
     * @param type Type to write out as
     * @param filename File name
     * @param root root path where file will be written
     * @throws IOException
     */
    public void writeMetadata(Resource md, MediaType type, String filename, Path root) throws IOException {
        Path rdfOutputTarget = root.resolve(filename);
        try (final OutputStream out = Files.newOutputStream(rdfOutputTarget, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)) {
            log.debug("Writing model to {}", rdfOutputTarget);
            this.writeResource(md, out, type);
        }
    }
}
