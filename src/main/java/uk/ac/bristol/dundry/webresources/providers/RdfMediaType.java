package uk.ac.bristol.dundry.webresources.providers;

import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;

public class RdfMediaType {

    private RdfMediaType() {
    }

    public final static String APPLICATION_RDF_XML = "application/rdf+xml";

    public final static MediaType APPLICATION_RDF_XML_TYPE =
            new MediaType("application", "rdf+xml");

    public final static String TEXT_RDF_N3 = "text/rdf+n3";

    public final static MediaType TEXT_RDF_N3_TYPE = new MediaType("text", "rdf+n3");

    public final static String APPLICATION_XML = "application/xml";
    
    public final static String APPLICATION_SPARQL = "application/sparql-results+xml";
    
    public final static MediaType APPLICATION_SPARQL_TYPE = new MediaType("application", "sparql-results+xml");

    public final static String TEXT_TURTLE = "text/turtle";
    
    public final static MediaType TEXT_TURTLE_TYPE = new MediaType("text", "turtle");
    
    public final static String TEXT_CSV = "text/csv";
    
    public final static MediaType TEXT_CSV_TYPE = new MediaType("text", "csv");
    
    public final static String TEXT_TSV = "text/tab-separated-values";
    
    public final static MediaType TEXT_TSV_TYPE = new MediaType("text", "tab-separated-values");
    
    public final static String APPLICATION_JSON_LD = "application/ld+json";
    
    public final static MediaType APPLICATION_JSON_LD_TYPE = new MediaType("application", "ld+json");
    
    public final static Map<String, MediaType> TYPE_LOOKUP = ImmutableMap.<String, MediaType>builder()
            .put(APPLICATION_RDF_XML, APPLICATION_RDF_XML_TYPE)
            .put(TEXT_RDF_N3, TEXT_RDF_N3_TYPE)
            .put(TEXT_TURTLE, TEXT_TURTLE_TYPE)
            .put(APPLICATION_JSON_LD, APPLICATION_JSON_LD_TYPE)
            .build();
            
}
