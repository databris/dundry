package uk.ac.bristol.dundry;

import java.nio.file.Path;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.engine.binding.BindingFactory;
import java.nio.file.Paths;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class UtilTest {
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    final static Model MODEL = ModelFactory.createDefaultModel();
    
    /**
     * Test of append method, of class Util.
     */
    @Test
    public void testResolve() {
        Resource res = r("http://example.com/foo/bar#baz");
        // Absolute
        assertEquals(r("http://example.com/x/y"), Util.append(res, Paths.get("/x/y")));
        // Relative
        assertEquals(r("http://example.com/foo/x/y"), Util.append(res, Paths.get("x/y")));
        // Contains a bad character
        assertEquals(r("http://example.com/foo/x/a%20space/y"), Util.append(res, Paths.get("x/a space/y")));
        assertEquals(r("http://example.com/foo/x/%5Bchunky%5D/y"), Util.append(res, Paths.get("x/[chunky]/y")));
        // Treats repo:123abc as repo:123abc/
        res = r("http://example.com/repo/1234abc");
        assertEquals(r("http://example.com/repo/1234abc/foo/x/y"), Util.append(res, Paths.get("foo/x/y")));
        // Resolves repo:123abc "" to repo:123abc
        assertEquals(r("http://example.com/repo/1234abc"), Util.append(res, Paths.get("")));
        // Hurls on bnode
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Cannot append against a blank node");
        Util.append(r(null), Paths.get("x/y")); 
    }
    
    @Test
    public void testToResource() {
        Model m = ModelFactory.createDefaultModel();
        
        assertThat(Util.toResource(m, Paths.get("foo")), is(r("foo")));
        
        assertThat(Util.toResource(m, Paths.get("/foo")), is(r("file:///foo")));
        
        assertThat(Util.toResource(m, Paths.get("foo/bar")), is(r("foo/bar")));
        
        assertThat(Util.toResource(m, Paths.get("foo bar/baz")), is(r("foo%20bar/baz")));
    }
    
    private Resource r(String uri) {
        if (uri == null) return MODEL.createResource();
        else return MODEL.createResource(uri);
    }
    
    @Test
    public void testLoadQuery() {
        Query q = Util.loadQuery("testqueries/simple.rq");
        
        assertThat(q, is(QueryFactory.create("select * where {?s ?p ?o}")));
    }
    
    @Test
    public void testPrebind() {
        Query q = QueryFactory.create("select * where {?s ?p ?o}");
        
        // No binding
        assertThat(Util.prebind(q, BindingFactory.binding()),
                is(QueryFactory.create("select * where { values () { () } { ?s ?p ?o } }")));
        
        // A binding
        Binding binding = BindingFactory.binding(
                Var.alloc("o"), 
                NodeFactory.createLiteral("test"));
        
        assertThat(Util.prebind(q, binding),
                is(QueryFactory.create("select * where { values ?o {'test'} { ?s ?p ?o } }")));
        
        // Check that the rest of the query is copied
        q = QueryFactory.create("describe ?s where {?s ?p ?o}");
        
        assertThat(Util.prebind(q, binding),
                is(QueryFactory.create("describe ?s where { values ?o {'test'} { ?s ?p ?o } }")));
    }
}
