/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.bristol.dundry.dao;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import org.hamcrest.Description;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentMatcher;

/**
 *
 * @author pldms
 */
public class FileSystemSourceTest {
    
    final String dir = "src/test/resources/filesource";
    
    @Test
    public void testGetTreeSize() throws IOException {
        String expected = loadFromFile("src/test/resources/dir-expected-noskip.txt");
        
        FileSystemSource fss = new FileSystemSource(dir);
        
        String result = fss.getTreeAt("", 3, 2, true).toString();
        
        assertThat(result, matches(expected));
    }
    
    @Test
    public void testGetTreeNoSize() throws IOException {
        String expected = loadFromFile("src/test/resources/dir-expected-skip.txt");
        
        FileSystemSource fss = new FileSystemSource(dir);
        
        String result = fss.getTreeAt("", 3, 2, false).toString();
        
        assertThat(result, matches(expected));
    }
    
    private String loadFromFile(String filename) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(filename));
        String match = new String(encoded, StandardCharsets.UTF_8);
        
        // Make this a regex match, with 'N' representing any digit
        match = match.replaceAll("N", "\\\\d");
        match = match.replaceAll("\\[", "\\\\[");
        match = match.replaceAll("\\]", "\\\\]");
                
        return match;
    }
    
    private static REMatcher matches(String regex) {
        return new REMatcher(regex);
    }
    
    static class REMatcher extends ArgumentMatcher<Object> {

    private final Pattern regex;

    REMatcher(String regex) {
        this.regex = Pattern.compile(regex);
    }
    
    @Override
    public boolean matches(Object actual) {
        return (actual instanceof String) && regex.matcher((String) actual).matches();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("regex(\"" + regex
                + "\")");
    }
}
}
