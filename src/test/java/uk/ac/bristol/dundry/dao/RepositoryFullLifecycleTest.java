package uk.ac.bristol.dundry.dao;

import com.github.jsonldjava.core.JsonLdError;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.DCTerms;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.quartz.SchedulerException;
import uk.ac.bristol.dundry.tasks.SyncTask;
import uk.ac.bristol.dundry.webresources.providers.RDFSerialiser;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class RepositoryFullLifecycleTest {

    private Path pubSpace;
    private Path pubSpaceLinks;
    
    @Before
    public void setUp() throws IOException {
        this.pubSpace = Files.createTempDirectory(Paths.get(System.getProperty("java.io.tmpdir")), "databris-fullcycle-");
        this.pubSpaceLinks = Files.createTempDirectory(Paths.get(System.getProperty("java.io.tmpdir")), "databris-fullcycle-links-");
    }
    
    @After
    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(pubSpace.toFile());
        FileUtils.deleteDirectory(pubSpaceLinks.toFile());
    }
    
    @Test
    public void fullPublicProcess() throws IOException, SchedulerException, JsonLdError, InterruptedException {
        MetadataStore mStore = new MetadataStoreDS(DatasetFactory.createTxnMem());
        
        FileRepository fRepo = new FileRepository(
                pubSpace.resolve("deposit").toString(),
                pubSpace.resolve("public").toString(),
                Arrays.asList(
                        pubSpace.resolve("restricted").toString(), 
                        pubSpace.resolve("controlled").toString()
                )
        );
        
        Properties props = new Properties();
        props.setProperty("zip.maxsize", "5000000");
        props.setProperty("indexpublish.templatebase", "/templates");
        props.setProperty("indexpublish.headertemplate", "header.ftl");
        props.setProperty("indexpublish.footertemplate", "footer.ftl");
        props.setProperty("indexpublish.headerfilename", ".header.html");
        props.setProperty("indexpublish.footerfilename", ".footer.html");
        props.setProperty("indexpublish.rdffilename", ".info.rdf");
        props.setProperty("indexpublish.jsonfilename", ".info.json");
        props.setProperty("publish.base", pubSpaceLinks.toString());
        
        Repository repo = new Repository(
                fRepo,
                mStore,
                Collections.singletonList("uk.ac.bristol.dundry.tasks.SyncTask"),
                Arrays.asList(
                        "uk.ac.bristol.dundry.tasks.IndexDepositTask", 
                        "uk.ac.bristol.dundry.tasks.MDExtractTika",
                        "uk.ac.bristol.dundry.tasks.ZipPreTask",
                        "uk.ac.bristol.dundry.tasks.ZipTask",
                        "uk.ac.bristol.dundry.tasks.PublishSymLinkTask",
                        "uk.ac.bristol.dundry.tasks.GenerateApacheIndexes"),
                Collections.EMPTY_LIST,
                props,
                props
        );
        
        repo.fileSystemSource = new FileSystemSource("src/test/resources/filesource");
        repo.taskManager = new TaskManager();
        repo.rdfSerialiser = new RDFSerialiser(
                Collections.EMPTY_MAP, 
                "http://vocab.bris.ac.uk/datasets.json", 
                "http://example.com/fpp/");
        
        Resource meta = ModelFactory.createDefaultModel().createResource();
        
        meta.addProperty(DCTerms.title, "The title");
        meta.addProperty(DCTerms.description, "A description");
        meta.addProperty(DC.date, "1066-12-25"); // will publish immediately
        
        String id = repo.create("xxxx", meta);
        
        System.out.println("ID: " + id);
        
        assertTrue("ID of right form", id.matches("[a-z0-9]{23,}"));
        
        assertThat(repo.getState(id), is(Repository.State.Created));
        
        meta = repo.getMetadata(id);
        
        assertTrue("Title there", meta.hasProperty(DCTerms.title, "The title"));
        
        repo.setSource(id, "b");
        
        // DANGER: could hang
        int count = 0;
        while (repo.getState(id) == Repository.State.Depositing && count < 20) {
            Thread.sleep(100);
            count += 1;
        }
        
        assertThat(repo.getState(id), is(Repository.State.Deposited));
        
        assertTrue("File synced", exists("deposit", id, "dir4", "file"));
        
        repo.publish(id);
        
        count = 0;
        while (repo.getState(id) == Repository.State.Publishing && count < 20) {
            Thread.sleep(200);
            count += 1;
        }
        
        assertThat(repo.getState(id), is(Repository.State.Published));
        
        assertTrue("File synced", exists("public", id, "dir4", "file"));
        assertTrue("Zip present", exists("public", id, id + ".zip"));
        assertTrue("Metadata present", exists("public", id, ".info.rdf"));
        assertTrue("JSON metadata present", exists("public", id, ".info.json"));
        
        Set<PosixFilePermission> currentPermissions = 
                Files.getPosixFilePermissions(repo.getPublishPathForId(id));
        assertTrue("Others can read publication", 
                currentPermissions.contains(PosixFilePermission.OTHERS_READ));
        assertTrue("Others can list publication", 
                currentPermissions.contains(PosixFilePermission.OTHERS_EXECUTE));
        
        repo.hide(id);
        
        Set<PosixFilePermission> newPermissions = 
                Files.getPosixFilePermissions(repo.getPublishPathForId(id));
        assertFalse("Others can't read publication",
                newPermissions.contains(PosixFilePermission.OTHERS_READ));
        assertFalse("Others can't list publication",
                newPermissions.contains(PosixFilePermission.OTHERS_EXECUTE));
    }
    
    private boolean exists(String... path) {
        Path p = pubSpace;
        
        // Paths.get almost does this
        for (String e: path) {
            p = p.resolve(e);
        }
        
        return Files.exists(p);
    }
}
