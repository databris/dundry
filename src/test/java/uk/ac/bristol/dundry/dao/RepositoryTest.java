package uk.ac.bristol.dundry.dao;

import com.github.jsonldjava.core.JsonLdError;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Properties;
import org.apache.jena.riot.RDFDataMgr;
import static org.hamcrest.CoreMatchers.is;
import org.joda.time.LocalDate;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.quartz.SchedulerException;
import uk.ac.bristol.dundry.dao.Repository.State;
import uk.ac.bristol.dundry.webresources.providers.RDFSerialiser;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class RepositoryTest {
    private final Repository instance;
    
    public RepositoryTest() throws IOException, JsonLdError {
        instance = getRepository("repository/ds.trig");
    }
    
    public static Repository getRepository(String datasetURL) throws IOException, JsonLdError {
        Dataset ds = RDFDataMgr.loadDataset(datasetURL);
        MetadataStore mStore = new MetadataStoreDS(ds);
        FileRepository fr = mock(FileRepository.class);
        when(fr.create(anyString(), anyString())).thenReturn(Paths.get("bar"));
        when(fr.depositPathForId(anyString(), anyString())).thenReturn(Paths.get("baz"));
        when(fr.publishPathForId(anyString(), anyString())).thenReturn(Paths.get("bag"));
        Repository repo = new Repository(fr, mStore, 
                Collections.EMPTY_LIST, Collections.EMPTY_LIST, 
                Collections.EMPTY_LIST, new Properties(), new Properties());
        
        repo.taskManager = mock(TaskManager.class);
        
        RDFSerialiser serialiser = mock(RDFSerialiser.class);
        when(serialiser.getDefaultPrefixes()).thenReturn(PrefixMapping.Standard);
        when(serialiser.getBase()).thenReturn("http://example.com/things/");
        repo.rdfSerialiser = serialiser;
        
        return repo;
    }
    
    private Model model(int i) {
        return FileManager.get().loadModel("repository/" + i + ".ttl");
    }
    
    /**
     * Test of getIds method, of class Repository.
     */
    @Test
    public void testGetIds() {
    }
    
    @Test
    public void testHasId() {
        assertTrue(instance.hasId("2"));
        assertFalse(instance.hasId("89"));
    }
    
    /**
     * Test of create method, of class Repository.
     */ 
    @Test
    public void testCreate() throws Exception {
        Model m = ModelFactory.createDefaultModel();
        Resource foo = m.createResource("http://example.com/temp");
        String result = instance.create("creator", foo);
        
        // Check we got a result
        assertNotNull(result);
    }
    
    @Test
    public void testDeposit() throws SchedulerException, InterruptedException {
                
        instance.setSource("2", "test-source");
        
        assertThat(instance.getFullMetadata("2").getRequiredProperty(DCTerms.source).getString(),
                is("test-source"));
        
        // Mock won't state change for us...
        instance.setState("2", State.Deposited);
        
        instance.setSource("2", "new-test-source");
        
        assertThat(instance.getFullMetadata("2").getRequiredProperty(DCTerms.source).getString(),
                is("new-test-source"));
    }
    
    /**
     * Test of getMetadata method, of class Repository.
     */
    @Test
    public void testGetMetadata() {
        Resource md = instance.getMetadata("1");
        assertEquals("http://example.com/things/1", md.getURI());
        
        
        
        //System.out.println("GOT " + model(1));
        
        //System.out.println("COMP " + md.getModel());
        
        assertTrue(model(1).isIsomorphicWith(md.getModel()));
    }
    
    @Test
    public void testGetMetadataIsMerged() {
        Resource md = instance.getFullMetadata("2");
        
        assertTrue(model(2).isIsomorphicWith(md.getModel()));
    }
    
    /**
     * Test of updateMetadata method, of class Repository.
     */
    @Test
    public void testUpdateMetadata() {
        Resource md = instance.getMetadata("2");
        md.addProperty(DCTerms.abstract_, "abs 2");
        instance.updateMetadata("2", md);
        
        md = instance.getMetadata("2");
        
        assertTrue(md.hasProperty(DCTerms.abstract_, "abs 2"));
    }
    
    @Test
    public void testUpdateMetadataProvImmutable() {
        Resource md = instance.getMetadata("2");
        
        md.removeAll(DCTerms.title); // should stick
        md.removeAll(DCTerms.description); // shouldn't
        
        instance.updateMetadata("2", md);
        
        md = instance.getFullMetadata("2");
        
        assertFalse(md.hasProperty(DCTerms.title));
        assertTrue(md.hasProperty(DCTerms.description));
    }
    
    /*
     * Check that mutations don't infect provenance
     * I was really bitten by a bug here
     */
    @Test
    public void testUpdateProvMutable() {
        Resource md = instance.getMetadata("2");
        
        md.addProperty(DCTerms.available, "foo");
        
        instance.updateMetadata("2", md);
        
        md = instance.getProvenanceMetadata("2");
        md.removeAll(DCTerms.description);
        instance.updateProvenanceMetadata("2", md);
        
        assertFalse(instance.getProvenanceMetadata("2").hasProperty(DCTerms.description));
        // This was failing due to a silly bug
        assertFalse(instance.getMetadata("2").hasProperty(DCTerms.description));
    }
    
    /**
     * Date was reported as malformed
     * @throws SchedulerException 
     */
    @Test
    public void testPublishDate() throws SchedulerException {
        instance.publish("3");
        // 1066 -- publish immediately
        assertThat(instance.getState("3"), is(State.Publishing));
        
        instance.publish("4");
        // 2999 -- publish pending
        assertThat(instance.getState("4"), is(State.PendingPublish));
        
        // Items today ought to be immediately published
        Resource md = instance.getMetadata("5");
        md.addProperty(DCTerms.available, LocalDate.now().toString());
        instance.updateMetadata("5", md);
        instance.publish("5");
        assertThat(instance.getState("5"), is(State.Publishing));
    }
    
    /**
     * Test of toExternalId method, of class Repository.
     */
    @Test
    public void testToExternalId() {
    }

    /**
     * Test of toInternalId method, of class Repository.
     */
    @Test
    public void testToInternalId() {
    }
}
