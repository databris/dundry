package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Model;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import org.apache.jena.riot.RDFDataMgr;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.xmlunit.builder.Input;
import static org.xmlunit.matchers.CompareMatcher.isIdenticalTo;
import static org.xmlunit.matchers.ValidationMatcher.valid;

/**
 *
 * @author pldms
 */
@RunWith(value = Parameterized.class)
public class DataCiteSubmitTest {
    
    private static XMLOutputFactory xof;
    private static Source schema;
    
    @BeforeClass
    public static void setUpXML() throws IOException {
        schema = Input.fromFile("src/test/resources/datacite/metadata.xsd").build();
        xof = XMLOutputFactory.newInstance();
    }
    
    @Parameterized.Parameters(name = "{0}")
    public static Collection<Path> cases() throws IOException {
        DirectoryStream<Path> testFiles = 
                Files.newDirectoryStream(Paths.get("src/test/resources/datacite"),
                        "*.ttl");
        
        Collection<Path> cases = new ArrayList<>();
        
        for (Path testFile: testFiles) {
            cases.add(testFile);
        }
        
        return cases;
    }
    
    private final Model data;
    private final String caseFilename;
    private final Source expected;
    
    public DataCiteSubmitTest(Path testFile) throws IOException {
        this.caseFilename = testFile.getFileName().toString();
        
        this.data = RDFDataMgr.loadModel(testFile.toUri().toString());
        
        // Work out the xml expected path
        String expectedName = caseFilename.replace(".ttl", ".xml");
        Path expectedFile = testFile.resolveSibling(expectedName);
        
        if (Files.notExists(expectedFile)) {
            throw new RuntimeException("Expected xml file " + expectedFile + " not found");
        }
        
        this.expected = Input.fromFile(expectedFile.toFile()).build();
    }
    
    @Test
    public void testToDataCite() throws Exception {
        String doi = "10.999/test";
        DataCiteSubmit instance = new DataCiteSubmit();
        
        StringWriter result = new StringWriter();
        XMLStreamWriter writer = //xof.createXMLStreamWriter(out);
                new IndentingXMLStreamWriter(xof.createXMLStreamWriter(result));
        
        instance.toDataCite(data.getResource("http://example.com/repo/1zqd7egla2qu1x3q5ky6zz6c3"), doi, writer);
        
        Source got = Input.fromString(result.toString()).build();
        
        assertThat(got, is(valid(schema)));
        
        assertThat(got, isIdenticalTo(expected));
    }
}
