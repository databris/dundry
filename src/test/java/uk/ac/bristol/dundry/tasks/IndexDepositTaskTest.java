package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.DCTypes;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import java.nio.file.Paths;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import uk.ac.bristol.dundry.dao.Repository;

/**
 *
 * @author pldms
 */
public class IndexDepositTaskTest {

    /**
     * Test of execute method, of class IndexDepositTask.
     */
    @Test
    public void testExecute() throws Exception {
        IndexDepositTask instance = new IndexDepositTask();
        
        Model initialState =
                FileManager.get().loadModel("src/test/resources/indexing/previously_indexed.ttl");  
        
        Resource initialProv = initialState.getResource("http://example.com/repo/abc");
        
        Repository repo = mock(Repository.class);
        
        when(repo.getDataPathForId(anyString())).thenReturn(Paths.get("src/test/resources/indexing/abc"));
        when(repo.getProvenanceMetadata(anyString())).thenReturn(initialProv);
        
        Model m = ModelFactory.createDefaultModel();
        Model toRemove = ModelFactory.createDefaultModel();
        
        Resource item = m.getResource("http://example.com/repo/abc");
        
        instance.execute(repo, null, null, item, toRemove, "abc", null, null);
                
        assertTrue(m.contains(item, DCTerms.hasPart, m.getResource("http://example.com/repo/abc/a")));
        
        // We don't want these on the root item -- they're just noise
        assertFalse("Root not typed as collection",
                m.contains(item, RDF.type, DCTypes.Collection));
        assertFalse("Root not labelled", item.hasProperty(RDFS.label));
        
        assertTrue("Folder typed as collection", 
                m.contains(m.getResource("http://example.com/repo/abc/a"), RDF.type, DCTypes.Collection));
        
        assertTrue("Folder labelled with name",
                m.contains(m.getResource("http://example.com/repo/abc/a"), RDFS.label, "a"));
        
        Model expected =
                FileManager.get().loadModel("src/test/resources/indexing/expected.ttl");     
        
        //System.out.println(expected);
        
        
        assertTrue("Expected index model matches", expected.isIsomorphicWith(m));
        
        Resource changedResource = toRemove.getResource("http://example.com/repo/abc/a/file2");
        Resource changedDataset = toRemove.getResource("http://example.com/repo/abc");
        
        //System.out.println("Change is: " + toRemove.size());
        
        toRemove.write(System.out, "TTL");
        
        assertTrue("Will remove three statements during reindex", toRemove.size() == 3);
        assertTrue("Remove size 1", changedResource.hasLiteral(DCTerms.extent, 1));
        assertTrue("Remove sha1 x", changedResource.hasLiteral(FOAF.sha1, "x"));
        assertTrue("Remove size 4 from dataset size", changedDataset.hasLiteral(DCTerms.extent, 4));
    }
    
}
