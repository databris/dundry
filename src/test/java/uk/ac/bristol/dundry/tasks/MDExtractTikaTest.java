package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;
import uk.ac.bristol.dundry.tasks.MDExtractTika.MDExtractVisitor;

/**
 *
 * @author pldms
 */
public class MDExtractTikaTest {
    
    public MDExtractTikaTest() {
    }

    /**
     * Test of execute method, of class MDExtractTika.
     */
    @Test
    public void testFileRecognition() throws IOException, URISyntaxException {
        String id = "http://example.com/1";
        Resource item = ModelFactory.createDefaultModel().createResource(id);
        Path root = Paths.get("/");
        MDExtractVisitor mdExtractor = new MDExtractVisitor(root, item, "zbcdef");
        
        URL isodat = getClass().getClassLoader().getResource("tika/1.cf");
        
        assertThat(isodat, not(nullValue()));
        
        mdExtractor.visitFile(Paths.get(isodat.toURI()), null);
        
        item.getModel().write(System.out, "TTL");
        
        assertThat(item.getModel().contains(null, DCTerms.format, "application/x-thermo-isodat"), is(true));
    }
    
}
