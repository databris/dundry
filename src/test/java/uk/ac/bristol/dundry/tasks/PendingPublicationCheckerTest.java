package uk.ac.bristol.dundry.tasks;

import com.github.jsonldjava.core.JsonLdError;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shared.PrefixMapping;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import uk.ac.bristol.dundry.dao.MetadataStore;
import uk.ac.bristol.dundry.dao.MetadataStoreDS;
import uk.ac.bristol.dundry.dao.Repository;
import uk.ac.bristol.dundry.webresources.providers.RDFSerialiser;

/**
 *
 * @author pldms
 */
public class PendingPublicationCheckerTest {

    /**
     * Test of publishPending method, of class PendingPublicationChecker.
     */
    @Test
    public void testPublishPending() throws JsonLdError {
        MetadataStore mdStore = 
                new MetadataStoreDS(RDFDataMgr.loadDataset("repository/pending.trig"));
        
        Repository repo = new Repository(null, mdStore, 
                Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST,
                new Properties(), new Properties());
        
        RDFSerialiser serialiser = mock(RDFSerialiser.class);
        when(serialiser.getDefaultPrefixes()).thenReturn(PrefixMapping.Standard);
        when(serialiser.getBase()).thenReturn("http://example.com/things/");
        repo.rdfSerialiser = serialiser;
        
        PendingPublicationChecker instance = new PendingPublicationChecker();
        
        List<String> toPublish = instance.findPublishPending(repo);
        
        System.out.println(toPublish);
        
        assertThat(toPublish.size(), is(2));
        assertThat(toPublish, hasItems("1", "2"));
    }
    
}
