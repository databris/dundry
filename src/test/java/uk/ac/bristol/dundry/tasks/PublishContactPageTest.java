package uk.ac.bristol.dundry.tasks;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.FileManager;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pldms
 */
public class PublishContactPageTest {
    /**
     * Test of copyReachable method, of class PublishContactPage.
     */
    @Test
    public void testCopyReachable() {
        PublishContactPage instance = new PublishContactPage();
        
        FileManager fm = FileManager.get();
        
        Model original = fm.loadModel("src/test/resources/restricted/full.rdf");
        Model expected = fm.loadModel("src/test/resources/restricted/trimmed.rdf");
        
        Resource start = original.getResource("http://example.com/repo/3xfulm6b89ow1on0gpwmmc4ic");
        
        Resource result = instance.copyReachable(start);
        
        assertThat(result.getURI(), is("http://example.com/repo/3xfulm6b89ow1on0gpwmmc4ic"));
        assertTrue("Trimmed as expected", expected.isIsomorphicWith(result.getModel()));
    }
    
}
