package uk.ac.bristol.dundry.tasks;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DCTerms;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.quartz.JobDataMap;
import uk.ac.bristol.dundry.Util;
import uk.ac.bristol.dundry.dao.Repository;

/**
 *
 * @author pldms
 */
public class ZipTaskTest {
    
    public ZipTaskTest() {
    }
    
    @Rule
    public TemporaryFolder folder= new TemporaryFolder();
    
    /**
     * Test of execute method, of class ZipTask.
     */
    @Test
    public void testExecuteWorks() throws Exception {
        ZipPreTask preinstance = new ZipPreTask();
        ZipTask instance = new ZipTask();
        
        JobDataMap data = new JobDataMap();
        data.put("zip.maxsize", 13L);
        
        Path target = folder.getRoot().toPath();
                
        Path source = getTestDir();
        
        // Copy test dir into temp dir
        Path root = Util.copyDirectory(source, target);
                
        Resource prov = ModelFactory.createDefaultModel().getResource("http://example.com/repo/abcd");
        
        preinstance.execute(null, null, null, prov, null, "abcd", root, data);
        // Bah, because we had to split this I need to pass on previous stage
        Repository repo = mock(Repository.class);
        when(repo.getProvenanceMetadata("abcd")).thenReturn(prov);
        instance.execute(repo, null, null, prov, null, "abcd", root, data);
        
        assertThat(Files.exists(root.resolve("abcd.zip")), is(true));
        
        assertThat(prov.hasProperty(DCTerms.hasFormat), is(true));
        
        assertThat(prov.getProperty(DCTerms.hasFormat).getObject().asResource(),
                is(ResourceFactory.createResource("http://example.com/repo/abcd/abcd.zip")));
        
        // List contents 
        ZipFile zip = new ZipFile(root.resolve("abcd.zip").toFile());
        
        Set<String> entries = Sets.newHashSet();
        
        Iterators.addAll(entries, 
                Iterators.transform(Iterators.forEnumeration(zip.entries()),
                        toName));
                
        Set<String> expect = ImmutableSet.of(
                "test-dir-size/",
                "test-dir-size/a/b/d/4-byte-file",
                "test-dir-size/a/b/c/4-byte-file",
                "test-dir-size/a/b/4-byte-file",
                "test-dir-size/a/b/",
                "test-dir-size/a/",
                "test-dir-size/a/b/d/",
                "test-dir-size/a/b/c/"
        );
        
        assertThat(entries, is((Set) expect));
    }
    
    // Get name of zip entries
    private static final Function<ZipEntry, String> toName = 
            new Function<ZipEntry, String>() {
        @Override
        public String apply(ZipEntry e) { return e.getName(); }
    };
    
    /**
     * Test of execute method, of class ZipTask.
     */
    @Test
    public void testExecuteDoesNowt() throws Exception {
        ZipPreTask preinstance = new ZipPreTask();
        ZipTask instance = new ZipTask();
        
        JobDataMap data = new JobDataMap();
        data.put("zip.maxsize", 12L);
        
        Path target = folder.getRoot().toPath();
                
        Path source = getTestDir();
        
        // Copy test dir into temp dir
        Path root = Util.copyDirectory(source, target);
        
        Resource prov = ModelFactory.createDefaultModel().getResource("http://example.com/repo/abcd");
        
        preinstance.execute(null, null, null, prov, null, "abcd", root, data);
        // Bah, because we had to split this I need to pass on previous stage
        Repository repo = mock(Repository.class);
        when(repo.getProvenanceMetadata("abcd")).thenReturn(prov);
        instance.execute(repo, null, null, prov, null, "abcd", root, data);
        
        assertThat(Files.exists(root.resolve("abcd.zip")), is(false));
        
        assertThat(prov.hasProperty(DCTerms.hasFormat), is(false));
    }
    
    @Test
    public void testDirectorySmallerThan() throws URISyntaxException, IOException {
        ZipPreTask preinstance = new ZipPreTask();
        ZipTask instance = new ZipTask();
        
        Path dir = getTestDir();
        
        assertThat(preinstance.directorySmallerThan(dir, 13), is(true));
        
        assertThat(preinstance.directorySmallerThan(dir, 12), is(false));
        
        assertThat(preinstance.directorySmallerThan(dir, 7), is(false));
    }
    
    @Test
    public void testGetZipAlternative() throws URISyntaxException, IOException {
        ZipTask instance = new ZipTask();
        
        Resource prov = ModelFactory.createDefaultModel().getResource("http://example.com/repo/abcd");
        
        assertThat(instance.getZipAlternative(prov), nullValue());
        
        Resource zipres = Util.append(prov, Paths.get("1abcd.zip"));
        zipres.addProperty(DCTerms.format, "application/zip");
        prov.addProperty(DCTerms.hasFormat, zipres);
        
        assertThat(instance.getZipAlternative(prov), is("1abcd.zip"));
        
    }

    private Path getTestDir() throws URISyntaxException {
        URL testres = ZipTaskTest.class.getResource("/zip/test-dir-size");
        assertThat(testres, not(nullValue()));
        Path dir = Paths.get(testres.toURI());
        return dir;
    }
}
