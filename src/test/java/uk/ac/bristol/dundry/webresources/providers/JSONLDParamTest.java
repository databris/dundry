package uk.ac.bristol.dundry.webresources.providers;

import com.github.jsonldjava.core.JsonLdError;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.json.JSONArray;
import org.json.JSONException;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

/**
 *
 * @author pldms
 */
@RunWith(value = Parameterized.class)
public class JSONLDParamTest {
    
    @Parameters(name = "{0}")
    public static Collection<Path[]> cases() throws IOException {
        DirectoryStream<Path> jsonldPaths = 
                Files.newDirectoryStream(Paths.get("src/test/resources/jsonld"),
                        "*.jsonld");
        
        Collection<Path[]> cases = new ArrayList<>();
        
        for (Path jsonldPath: jsonldPaths) {
            String filename = jsonldPath.getFileName().toString();
            Path turtlePath = jsonldPath.getParent().resolve(filename.replace(".jsonld", ".ttl"));
            
            cases.add(new Path[]{jsonldPath.getFileName(), jsonldPath, turtlePath});
        }
        
        return cases;
    }
    
    private final Path json;
    private final Path turtle;
    private final JSONLDWriter jsonWriter;
    private final String expectedJSON;
    private final Model expectedRDF;
    private final Path name;
   
    public JSONLDParamTest(Path name, Path json, Path turtle) throws JsonLdError, IOException {
        this.name = name;
        this.json = json;
        this.turtle = turtle;
        this.jsonWriter = new JSONLDWriter(
                "http://vocab.bris.ac.uk/datasets.json",
                "http://example.com/things/");
        this.expectedJSON = readFile(json.toString());
        this.expectedRDF = RDFDataMgr.loadModel(turtle.toUri().toString());
    }
    
    @Test
    public void writingJSON() throws IOException, JSONException {
        Resource root = expectedRDF.getResource("http://example.com/things/1234abcd");
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        jsonWriter.write(root, out);
        
        String result = out.toString("UTF-8");
        
        //dump(expectedJSON, result);
                
        CustomComparator comparator = 
                new CustomComparator(JSONCompareMode.STRICT,
                        // We don't care about order for subjects
                        new Customization("subjects", 
                                JSONLDParamTest::orderInsensitiveCompare) 
                );
        
        //dump(expectedJSON, result);
        
        JSONAssert.assertEquals(expectedJSON, result, comparator);
    }
    
    @Test
    public void readingJSON() throws IOException, JSONException {
        
        StringReader r = new StringReader(expectedJSON);
        
        Model got = ModelFactory.createDefaultModel();
        RDFDataMgr.read(got, r, "http://example.com/things/", Lang.JSONLD);
        
        boolean match = expectedRDF.isIsomorphicWith(got);
        
        if (!match) {
            dump(expectedRDF, got);
        }
        
        assertTrue("Models are isomorphic", match);
    }
    
    private String readFile(String filename) throws IOException {
        // RDFDataMgr does classpath opening too, which is great
        try (InputStream in = RDFDataMgr.open(filename);) {
            return IOUtils.toString(in, "UTF-8");
        }
    }
    
    private void dump(Model expected, Model got) {
        System.out.printf("==== Expected %s ====\n", name);
        expected.write(System.out, "TTL");
        System.out.println("==== Got ====");
        got.write(System.out, "TTL");
        System.out.println("========");
        System.out.flush();
    }
    
    private void dump(String expected, String r) {
        System.out.printf("==== Expected %s ====\n", name);
        System.out.println(expected);
        System.out.println("==== Got ====");
        System.out.println(r);
        System.out.println("========");
        System.out.flush();
    }
    
    private static boolean orderInsensitiveCompare(Object a, Object b) {

        if (!(a instanceof JSONArray) || !(b instanceof JSONArray)) {
            return a.equals(b);
        }

        JSONArray aj = (JSONArray) a;
        JSONArray bj = (JSONArray) b;

        if (aj.length() != bj.length()) {
            return false;
        }
        
        // Copy b
        List<Object> copy = new ArrayList<>(bj.length());
        for (int i = 0; i < bj.length(); i++) {
            try {
                copy.add(bj.get(i));
            } catch (JSONException ex) {
                throw new RuntimeException(ex);
            }
        }
                
        // QUADRATIC!
        for (int i = 0; i < aj.length(); i++) {
            boolean found = false;
            for (int j = 0; j < copy.size(); j++) {
                try {
                    if (aj.get(i).equals(copy.get(j))) {
                        found = true;
                        copy.remove(j); // We've matched, so remove match
                        break;
                    }
                } catch (JSONException ex) {
                    throw new RuntimeException(ex);
                }
            }
            
            if (!found) {
                return false;
            }
        }

        return true;

    }
}
