package uk.ac.bristol.dundry.webresources.providers;

import com.github.jsonldjava.core.JsonLdError;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import java.io.ByteArrayOutputStream;
import javax.ws.rs.core.MediaType;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 *
 * @author pldms
 */
public class RDFSerialiserTest {
    
    public RDFSerialiserTest() {
    }

    @Test
    public void testWriteModel() throws JsonLdError {
        String pre1 = "http://example.com/1/";
        String pre2 = "http://example.com/2/";
        
        Model model = ModelFactory.createDefaultModel();
        model.add(ResourceFactory.createResource(pre1 + "a"),
                ResourceFactory.createProperty(pre2 + "b"),
                ResourceFactory.createResource());
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MediaType mediaType = RdfMediaType.TEXT_TURTLE_TYPE;
        ImmutableMap.Builder prefixes = ImmutableMap.builder();
        
        RDFSerialiser instance = new RDFSerialiser(
                prefixes.put("pre1", pre1).put("pre2", pre2).build(),
                "http://vocab.bris.ac.uk/datasets.json", "http://example.com/things/"
        );
        
        instance.writeModel(model, outputStream, RdfMediaType.TEXT_TURTLE_TYPE);
        
        String result = new String(outputStream.toByteArray(), Charsets.UTF_8);
        
        assertThat(result, containsString("@prefix pre1:  <" + pre1 + ">"));
        assertThat(result, containsString("@prefix pre2:  <" + pre2 + ">"));
    }
    
}
