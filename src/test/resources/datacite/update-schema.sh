#!/bin/sh

#set -x
set -e

VERSION=$1

echo Fetching version $VERSION

curl -f -s -O https://schema.datacite.org/meta/kernel-$VERSION/metadata.xsd

echo Removing includes

rm -f include/*.xsd

INCS=$(grep '<xs:include' metadata.xsd | cut -d '"' -f 2)

echo Found includes: $INCS

cd include

for INC in $INCS
do
	echo Fetching $INC
	curl -f -s -O https://schema.datacite.org/meta/kernel-$VERSION/$INC
done
