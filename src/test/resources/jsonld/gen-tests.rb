# Quick script to generate tests for JSONLD serialising / deserialising

Prop = Struct.new(
	:short_name, 
	:prefixed_name, 
	:is_literal?, 
	:is_multivalued?, 
	:is_ordered?
	)

props = [
	Prop.new('title', 'dc:title', true, false, false),
	Prop.new('description', 'dc:description', true, false, false),
	Prop.new('funder', 'repo:funder', false, false, false),
	Prop.new('subjects', 'dc:subject', true, true, false),
	Prop.new('creators', 'dc:creator', false, true, true)
]

def make_lit_vals(prop, index, num)

	num.times.map { |i| "val_#{index}_#{i}" }

end

def make_res_val(prop, index, num)

	case (num + index) % 3
	when 0 then { 'id': "#{index}_#{num}", label: "label_#{index}_#{num}"}
	when 1 then { label: "label_#{index}" }
	else { 'id': "#{index}_#{num}" }
	end
	
end

def make_res_vals(prop, index, num)

	num.times.map { |i| make_res_val(prop, index, i) }

end
	
test_data = {}

props.each do |prop|

	test_data_for_prop = []

	test_data[prop] = test_data_for_prop

	if prop.is_literal?
		test_data_for_prop << make_lit_vals(prop, 0, 1)

		if prop.is_multivalued?
			test_data_for_prop << make_lit_vals(prop, 0, 3)
		end
	else
		test_data_for_prop << make_res_vals(prop, 0, 1)
		test_data_for_prop << make_res_vals(prop, 1, 1)
		test_data_for_prop << make_res_vals(prop, 2, 1)

		if prop.is_multivalued?
			test_data_for_prop << make_res_vals(prop, 0, 3)
		end
	end

end

def each_comb(test_data, keys, parent_vals, &block)

	keys = keys.clone # protect arg
	key = keys.shift # yes, this modifies keys

	if !key

		yield parent_vals

		return
	end

	test_data[key].each do |val|

		# Could use much more efficient tree struct here
		vals = parent_vals.clone
		vals[key] = val

		each_comb(test_data, keys, vals, &block)

	end

end

def to_json_val(val)

	return "\"#{val}\"" unless Hash === val

	ret = "{ "
	val.each_with_index do |kv,i|
		ret += "\"#{kv[0]}\": \"#{kv[1]}\""
		if i != val.size - 1
			ret += ", "
		end
	end
	ret += " }"
	ret
end


def write_json(vals, count)

	File.open("#{count}.jsonld", 'w') do |file|

		file.puts <<HEADER
{
  "@context": "http://vocab.bris.ac.uk/datasets.json",
  "@base": "http://example.com/things/",
  "id": "1234abcd",

HEADER

		vals.each_with_index do |pv, i|
			prop = pv[0]
			val = pv[1]
			file.print "  \"#{prop.short_name}\": "
			# If it must be an array, or has multiple elements
			if prop.is_multivalued? or val.size > 1
				file.puts "["

				file.print '    '

				file.print val.map {|h| to_json_val(h)}.join ', '

				file.print "\n  ]"
			else
				file.print to_json_val(val[0])
			end

			if i != vals.size - 1
				file.puts ","
			end
		end
		file.puts "\n}"
	end

end

def to_turtle_val(val, labels)

	return "\"#{val}\"" unless Hash === val

	if val.has_key? :'id'
		labels[val[:'id']] = val[:label] if val.has_key? :label
		"<#{val[:'id']}>"
	else
		"[ rdfs:label \"#{val[:label]}\" ]"
	end
end

def write_turtle(vals, count)

	File.open("#{count}.ttl", 'w') do |file|

		labels = {}

		file.puts <<HEADER
@base <http://example.com/things/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix repo: <http://vocab.bris.ac.uk/data/repository#> .

<1234abcd>
HEADER

		vals.each_with_index do |pv, i|
			prop = pv[0]
			val = pv[1]
			file.print "  #{prop.prefixed_name} "

			if prop.is_ordered?
				file.print "( "
			end

			if val.size > 1
				# Interesting irregularity in turtle
				joiner = if prop.is_ordered? then ' ' else ', ' end

				file.print val.map {|h| to_turtle_val(h, labels)}.join joiner
			else
				file.print to_turtle_val(val[0], labels)
			end

			if prop.is_ordered?
				file.print " )"
			end

			if i != vals.size - 1
				file.puts " ;"
			else
				file.puts " ."
			end
		end

		file.puts

		labels.each do |obj, label|
			file.puts "<#{obj}> rdfs:label \"#{label}\" ."
		end
	end

end

count = 0

each_comb(test_data, test_data.keys, {}) do |vals|

	write_json(vals, count)

	write_turtle(vals, count)

	count += 1
end
	

